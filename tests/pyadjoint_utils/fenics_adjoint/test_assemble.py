import ufl.form
from fenics import *
from fenics_adjoint import *
from pyadjoint import ReducedFunctional, Control
from pyadjoint_utils.fenics_adjoint.assembly import assemble, OverloadedUFLArgument


class FunctionWithFormAdjoint(Function, OverloadedUFLArgument):
    pass


def test_assemble_scalar():

    mesh = UnitSquareMesh(9, 9)
    V = FunctionSpace(mesh, "P", 1)
    u = Function(V)

    F = inner(grad(u), grad(u)) * dx

    E = assemble(F)

    J = ReducedFunctional(E, Control(u))
    d = J.derivative()
    assert isinstance(d, Function)

    u2 = FunctionWithFormAdjoint(V)

    F2 = inner(grad(u2), grad(u2)) * dx
    E2 = assemble(F2)

    J2 = ReducedFunctional(E2, Control(u2))
    d2 = J2.derivative(options={"riesz_representation": lambda x: x})
    assert isinstance(d2, ufl.form.Form)
