from pyadjoint import *
from pyadjoint_utils import *
import numpy as np
import pytest
from numpy.testing import assert_approx_equal, assert_allclose

solver_parameters = {"jmat_type": "action", "adj_jmat_type": "action"}

# Solve x^3 = 8.
def test_scalar():
    in_1 = AdjFloat(3.0)
    target = (8,)

    with push_tape():
        out_1 = in_1 * in_1 * in_1
        res_1 = target[0] - out_1
        controls = [Control(in_1)]
        residual = ReducedFunction([res_1], controls)

    reduced_equation = ReducedEquation(residual)

    solver = SNESSolver(reduced_equation, parameters=solver_parameters)
    solution = solver.solve(controls)

    assert_allclose(solution, [2.0])
    assert_allclose(residual(solution), [0], atol=1e-7)


# Solve a linear system.
def test_linear():
    in_1 = AdjFloat(3.0)
    in_2 = AdjFloat(5.0)
    target = (4, 7)

    with push_tape():
        out_1 = 2 * in_1 + in_2
        out_2 = 3 * in_1 + 2 * in_2

        res_1 = target[0] - out_1
        res_2 = target[1] - out_2
        controls = [Control(in_1), Control(in_2)]
        residual = ReducedFunction([res_1, res_2], controls)

    reduced_equation = ReducedEquation(residual)

    solver = SNESSolver(reduced_equation, parameters=solver_parameters)
    solution = solver.solve(controls, ksp_type="gmres")

    assert_allclose(solution, [1.0, 2.0])
    assert_allclose(residual(solution), [0, 0], atol=1e-7)


# Solve a nonlinear system.
def test_nonlinear():
    in_1 = AdjFloat(3.0)
    in_2 = AdjFloat(5.0)
    in_3 = AdjFloat(7.0)
    target = (2, 6, 3)

    with push_tape():
        out_1 = in_1 * in_1 * in_2
        out_2 = in_3 * in_2
        out_3 = in_1 * in_3

        res_1 = target[0] - out_1
        res_2 = target[1] - out_2
        res_3 = target[2] - out_3
        controls = [Control(in_1), Control(in_2), Control(in_3)]
        residual = ReducedFunction([res_1, res_2, res_3], controls)

    reduced_equation = ReducedEquation(residual)

    solver = SNESSolver(reduced_equation, parameters=solver_parameters)
    solution = solver.solve(controls, ksp_type="gmres")

    assert_allclose(solution, [1.0, 2.0, 3.0])
    assert_allclose(residual(solution), [0, 0, 0], atol=1e-7)

    return solution


# Solve a system with a subset of the Controls.
def test_controls_subset():
    in_1 = AdjFloat(3.0)
    in_2 = AdjFloat(5.0)
    in_3 = AdjFloat(3.0)
    target = (3, 6)

    with push_tape():
        out_1 = in_1 * in_1 + in_2
        out_2 = in_3 * in_2

        res_1 = target[0] - out_1
        res_2 = target[1] - out_2
        controls = [Control(in_1), Control(in_2)]
        residual = ReducedFunction([res_1, res_2], controls)

    reduced_equation = ReducedEquation(residual)

    solver = SNESSolver(reduced_equation, parameters=solver_parameters)
    solution = solver.solve(controls, ksp_type="gmres")

    assert_allclose(solution, [1.0, 2.0])
    assert_allclose(residual(solution), [0, 0], atol=1e-7)

    # Recompute using Control.
    rf = ReducedFunction(solution, Control(in_3))
    new_solution = rf(AdjFloat(-1))
    assert_allclose(new_solution, [3, -6])
    assert_allclose(residual(new_solution), [0, 0], atol=1e-7)

    new_solution = rf(AdjFloat(-6))
    assert_allclose(new_solution, [2, -1])
    assert_allclose(residual(new_solution), [0, 0], atol=1e-7)


def test_recompute():
    solution = test_nonlinear()
    rf = ReducedFunction(solution, [])

    new_solution = rf([])

    assert_allclose(solution, new_solution)


def test_adjoint():
    in_1 = AdjFloat(3.0)
    in_2 = AdjFloat(5.0)
    in_3 = AdjFloat(3.0)
    target = (3, 6)

    with push_tape():
        out_1 = in_1 * in_1 + in_2
        out_2 = in_3 * in_2

        res_1 = target[0] - out_1
        res_2 = target[1] - out_2
        controls = [Control(in_1), Control(in_2)]
        residual = ReducedFunction([res_1, res_2], controls)

    reduced_equation = ReducedEquation(residual)

    solver = SNESSolver(reduced_equation, parameters=solver_parameters)
    solution = solver.solve(controls, ksp_type="gmres")

    assert_allclose(solution, [1.0, 2.0])
    assert_allclose(residual(solution), [0, 0], atol=1e-7)

    in_3_con = Control(in_3)
    h = AdjFloat(1)
    assert taylor_test(ReducedFunctional(solution[0], in_3_con), in_3, h) > 1.9
    assert taylor_test(ReducedFunctional(solution[1], in_3_con), in_3, h) > 1.9


def test_minimize():
    in_1 = AdjFloat(3.0)
    in_2 = AdjFloat(5.0)
    in_3 = AdjFloat(6.0)
    target = (3, 6)

    with push_tape():
        out_1 = in_1 * in_1 * in_1 + in_2
        out_2 = in_3 * in_2

        res_1 = target[0] - out_1
        res_2 = target[1] - out_2
        controls = [Control(in_1), Control(in_2)]
        residual = ReducedFunction([res_1, res_2], controls)

    reduced_equation = ReducedEquation(residual)

    solver = SNESSolver(reduced_equation, parameters=solver_parameters)
    solution = solver.solve(controls, ksp_type="gmres")

    assert_allclose(solution, [2 ** (1 / 3), 1.0])
    assert_allclose(residual(solution), [0, 0], atol=1e-7)

    obs = (1, 2)
    J = (obs[0] - solution[0]) ** 2 + (obs[1] - solution[1]) ** 2

    in_3_con = Control(in_3)
    Jhat = ReducedFunctional(J, in_3_con)

    h = AdjFloat(1)
    assert taylor_test(Jhat, in_3, h) > 1.9

    in_3_opt = minimize(Jhat, options={"disp": False})
    assert_approx_equal(in_3_opt, 3)


# Test forward solve with Dirichlet boundary conditions.
def test_bc():
    in_1 = AdjFloat(3.0)
    in_2 = AdjFloat(5.0)
    in_3 = AdjFloat(7.0)
    in_4 = AdjFloat(4.0)
    target = (2, 6, 3, 1)

    with push_tape():
        out_1 = in_1 * in_1 * in_2 * in_4
        out_2 = in_3 * in_2 * in_4
        out_3 = in_1 * in_3 + in_4 - in_1 * in_1
        out_4 = in_1 + in_3 * in_3 * in_4

        res_1 = target[0] - out_1
        res_2 = target[1] - out_2
        res_3 = target[2] - out_3
        res_4 = target[3] - out_4

        controls = [Control(in_1), Control(in_2), Control(in_3), Control(in_4)]
        residual = ReducedFunction([res_1, res_2, res_3, res_4], controls)

    def bc(inputs):
        inputs[3] = target[3]
        return inputs

    def h_bc(inputs):
        inputs[3] = 0.0
        return inputs

    reduced_equation = ReducedEquation(residual, bc, h_bc)

    solver = SNESSolver(reduced_equation, parameters=solver_parameters)
    solution = solver.solve(
        controls, rtol=1e-10, stol=1e-10, max_it=100, ksp_type="gmres", disp=False
    )

    assert_allclose(solution, [1.0, 2.0, 3.0, 1.0])
    assert_allclose(h_bc(residual(solution)), [0, 0, 0, 0], atol=1e-7)

    return solution


# Test adjoint solve with Dirichlet boundary conditions.
def test_bc_adjoint():
    p = AdjFloat(2.0)
    in_1 = AdjFloat(3.0)
    in_2 = AdjFloat(5.0)
    in_3 = AdjFloat(7.0)
    in_4 = AdjFloat(4.0)
    target = (2, 6, 3, 1)

    with push_tape():
        out_1 = (in_1 ** p) * in_2 * in_4
        out_2 = ((in_3 * in_2) ** (p - 1)) * in_4
        out_3 = in_1 * in_3 + in_4 - in_1 ** (p + 1)
        out_4 = in_1 + (in_3 ** p) * in_4

        res_1 = target[0] - out_1
        res_2 = target[1] - out_2
        res_3 = target[2] - out_3
        res_4 = target[3] - out_4

        controls = [Control(in_1), Control(in_2), Control(in_3), Control(in_4)]
        residual = ReducedFunction([res_1, res_2, res_3, res_4], controls)

    def bc(inputs):
        inputs[3] = target[3]
        return inputs

    def h_bc(inputs):
        inputs[3] = 0.0
        return inputs

    reduced_equation = ReducedEquation(residual, bc, h_bc)

    solver = SNESSolver(reduced_equation, parameters=solver_parameters)
    solution = solver.solve(
        controls, rtol=1e-10, stol=1e-10, max_it=100, ksp_type="gmres", disp=False
    )

    assert_allclose(solution, [1.0, 2.0, 3.0, 1.0])
    assert_allclose(h_bc(residual(solution)), [0, 0, 0, 0], atol=1e-7)

    pcon = Control(p)
    h = AdjFloat(1e-1)
    assert taylor_test(ReducedFunctional(solution[0], pcon), p, h) > 1.9
    assert taylor_test(ReducedFunctional(solution[1], pcon), p, h) > 1.9
    assert taylor_test(ReducedFunctional(solution[2], pcon), p, h) > 1.9

    return solution


@pytest.mark.skip(reason="TODO: write a test using an assembled Jacobian")
def test_assembled_jacobian():
    pass


if __name__ == "__main__":
    test_scalar()
    test_linear()
    test_nonlinear()
    test_recompute()
    test_controls_subset()
    test_adjoint()
    test_minimize()
