import pytest

tf = pytest.importorskip("tensorflow.compat.v1")
fenics = pytest.importorskip("fenics")

import numpy as np

from fenics import *
from fenics_adjoint import *
from pyadjoint_utils.fenics_adjoint import *

try:
    from pyadjoint_utils.tensorflow_adjoint import *
except:
    pass
from pyadjoint import *
from pyadjoint_utils import *

from numpy.testing import assert_approx_equal, assert_allclose


def test_scalar_function_input():
    mesh = UnitSquareMesh(8, 8)
    V = FunctionSpace(mesh, "P", 1)
    u = Function(V)

    n = len(u.vector())
    data = np.arange(n) + 1

    u.vector()[:] = data
    p = AdjFloat(2)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_output = g_data ** g_p

        def outputs_conversion(outputs):
            f = Function(V)
            f.vector()[:] = outputs[0]
            return [f]

        def adj_outputs_conversion(adj_outputs):
            adj_u = Function(V)
            adj_u.vector()[:] = adj_outputs[g_data]
            adj_outputs[g_data] = adj_u.vector()
            return adj_outputs

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(
                g_output,
                {g_data: u, g_p: p},
                outputs_conversion=outputs_conversion,
                adj_outputs_conversion=adj_outputs_conversion,
            )
        J = assemble(output * output * dx)

        rf_u = ReducedFunction(output, Control(u))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(u)])

        Jhat_u = ReducedFunctional(J, Control(u))
        Jhat_p = ReducedFunctional(J, Control(p))
        Jhat_both = ReducedFunctional(J, [Control(p), Control(u)])

    correct_output = data ** p
    assert_allclose(output.vector()[:], correct_output)

    ### Test u input.
    data_rev = np.arange(n)[::-1]
    u_rev = Function(V)
    u_rev.vector()[:] = data_rev
    correct_output = data_rev ** p
    assert_allclose(rf_u(u_rev).vector()[:], correct_output)

    h = project(Expression("x[0] + x[1]*x[0] + x[0]*x[0]", degree=1) * 1e-1, V)
    assert taylor_test(Jhat_u, u, h) > 1.9

    ### Test p input.
    rf_u(u)  # Set data's block variable back to original u.
    correct_output = data ** 3
    assert_allclose(rf_p(3).vector()[:], correct_output)

    h = AdjFloat(1e-3)
    assert taylor_test(Jhat_p, p, h) > 1.9

    ### Test u and p inputs.
    correct_output = data_rev ** 4
    assert_allclose(rf_both([4, u_rev]).vector()[:], correct_output)

    h = [
        AdjFloat(1e-3),
        project(Expression("x[0] + x[1]*x[0] + x[0]*x[0]", degree=1) * 1e-1, V),
    ]
    assert taylor_test(Jhat_both, [p, u], h) > 1.9

    sess.close()


def test_vector_function_input():
    mesh = UnitSquareMesh(8, 8)
    V_vec = VectorFunctionSpace(mesh, "P", 1)
    gradu = Function(V_vec)

    n = len(gradu.vector())
    shape = (-1, 2)
    data = np.reshape(np.arange(n) + 1, shape)

    gradu.vector()[:] = data.flatten()
    p = AdjFloat(0.5)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_scale = g_data * g_data
            with tf.name_scope("reduce_sum"):
                g_scale = tf.reduce_sum(g_scale, axis=1) + 1e-12

            g_output = tf.expand_dims(g_scale ** (g_p / 2), 1) * g_data

        def inputs_conversion(feed_dict):
            # Convert gradu to a numpy array of the right shape.
            gradu = feed_dict[g_data]
            gradu_np = np.reshape(gradu.vector()[:], shape)
            feed_dict[g_data] = gradu_np
            return feed_dict

        def adj_inputs_conversion(adj_inputs):
            adj_inputs[0] = adj_inputs[0][:].reshape(shape)
            return adj_inputs

        def outputs_conversion(outputs):
            f = Function(V_vec)
            f.vector()[:] = outputs[0].flatten()
            return [f]

        def adj_outputs_conversion(adj_outputs):
            adj_gradu = Function(V_vec)
            adj_gradu.vector()[:] = adj_outputs[g_data].flatten()
            adj_outputs[g_data] = adj_gradu.vector()
            return adj_outputs

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(
                g_output,
                {g_data: gradu, g_p: p},
                inputs_conversion=inputs_conversion,
                outputs_conversion=outputs_conversion,
                adj_inputs_conversion=adj_inputs_conversion,
                adj_outputs_conversion=adj_outputs_conversion,
            )
        J = assemble(inner(output, output) * dx)

        rf_gradu = ReducedFunction(output, Control(gradu))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(gradu)])

        Jhat_gradu = ReducedFunctional(J, Control(gradu))
        Jhat_p = ReducedFunctional(J, Control(p))
        Jhat_both = ReducedFunctional(J, [Control(p), Control(gradu)])

    def numpy_func(p, data):
        scale = np.sum(data * data, axis=1) + 1e-12
        return ((scale ** (p / 2))[:, None] * data).flatten()

    correct_output = numpy_func(p, data)
    assert_allclose(output.vector()[:], correct_output)

    ### Test gradu input.
    data_rev = np.reshape(np.arange(n)[::-1], shape)
    gradu_rev = Function(V_vec)
    gradu_rev.vector()[:] = data_rev.flatten()
    correct_output = numpy_func(p, data_rev)
    assert_allclose(rf_gradu(gradu_rev).vector()[:], correct_output)

    h_gradu = project(Expression(("x[0]+x[1]", "x[1]*x[0]"), degree=1) * 1e-1, V_vec)
    assert taylor_test(Jhat_gradu, gradu, h_gradu) > 1.9

    ### Test p input.
    rf_gradu(gradu)  # Set data's block variable back to original u.
    new_p = 1
    correct_output = numpy_func(new_p, data)
    assert_allclose(rf_p(new_p).vector()[:], correct_output)

    h_p = AdjFloat(1e-3)
    assert taylor_test(Jhat_p, p, h_p) > 1.9

    ### Test u and p inputs.
    new_p = 2
    correct_output = numpy_func(new_p, data_rev)
    assert_allclose(rf_both([new_p, gradu_rev]).vector()[:], correct_output)

    assert taylor_test(Jhat_both, [p, gradu], [h_p, h_gradu]) > 1.9

    sess.close()


@pytest.mark.parametrize("use_two_blocks", [False, True])
def test_scalar_function_jacobian(use_two_blocks):
    mesh = UnitSquareMesh(8, 8)
    V = FunctionSpace(mesh, "P", 1)
    u = Function(V)

    n = len(u.vector())
    data = np.arange(n) + 1

    u.vector()[:] = data
    p = AdjFloat(2)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_output = g_data ** g_p

        def outputs_conversion(outputs):
            f = Function(V)
            f.vector()[:] = outputs[0]
            return [f]

        def adj_outputs_conversion(adj_outputs):
            adj_u = Function(V)
            adj_u.vector()[:] = adj_outputs[g_data]
            adj_outputs[g_data] = adj_u.vector()
            return adj_outputs

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(
                g_output,
                {g_data: u, g_p: p},
                outputs_conversion=outputs_conversion,
                adj_outputs_conversion=adj_outputs_conversion,
            )

            if use_two_blocks:
                output = run_tensorflow_graph(
                    g_output,
                    {g_data: output, g_p: p},
                    outputs_conversion=outputs_conversion,
                    adj_outputs_conversion=adj_outputs_conversion,
                )

        rf_u = ReducedFunction(output, Control(u))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(u)])

    if use_two_blocks:
        p2 = p * p
        correct_output = data ** p2
        correct_jac_p = (data ** p2) * np.log(data) * 2 * p
        correct_jac_u = np.diag(data ** (p2 - 1) * p2)
    else:
        correct_output = data ** p
        correct_jac_p = data ** p * np.log(data)
        correct_jac_u = np.diag(data ** (p - 1) * p)

    assert_allclose(output.vector()[:], correct_output)

    jac = rf_p.jac_matrix()
    assert isinstance(jac, np.ndarray)
    assert_allclose(jac.flatten(), correct_jac_p.flatten())
    assert jac.shape == (n,)

    jac = rf_u.jac_matrix()
    assert isinstance(jac, np.ndarray)
    assert_allclose(jac.flatten(), correct_jac_u.flatten())
    assert jac.shape == (n, n)

    jacs = rf_both.jac_matrix()
    assert isinstance(jacs, (list, tuple))
    assert len(jacs) == 2
    assert_allclose(jacs[0], correct_jac_p)
    assert_allclose(jacs[1], correct_jac_u)

    # Test Jacobian action.
    h_p = AdjFloat(1e-3)
    h_u = project(Expression("x[0] + x[1]*x[0] + x[0]*x[0]", degree=1) * 1e-1, V)
    h_u_np = h_u.vector()[:]
    h_both = [h_p, h_u_np]

    jac_action = rf_p.jac_action(h_p)
    correct_jac_action_p = np.tensordot(correct_jac_p, h_p, axes=0)
    assert_allclose(jac_action.vector()[:], correct_jac_action_p.flatten())

    jac_action = rf_u.jac_action(h_u_np)
    correct_jac_action_u = np.tensordot(correct_jac_u, h_u_np, axes=len(h_u_np.shape))
    assert_allclose(jac_action.vector()[:], correct_jac_action_u.flatten())

    jac_action = rf_both.jac_action(h_both)
    correct_jac_action_both = correct_jac_action_p + correct_jac_action_u
    assert_allclose(jac_action.vector()[:], correct_jac_action_both.flatten())

    sess.close()


@pytest.mark.parametrize("use_two_blocks", [False, True])
def test_vector_function_jacobian(use_two_blocks):
    mesh = UnitSquareMesh(8, 8)
    V_vec = VectorFunctionSpace(mesh, "P", 1)
    gradu = Function(V_vec)

    n_vec = len(gradu.vector())
    n = n_vec // 2
    shape = (-1, 2)
    data = np.reshape(np.arange(n_vec) + 1, shape)

    gradu.vector()[:] = data.flatten()
    p = AdjFloat(0.5)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_scale = g_data * g_data
            with tf.name_scope("reduce_sum"):
                g_scale = tf.reduce_sum(g_scale, axis=1) + 1e-12

            g_output = tf.expand_dims(g_scale ** (g_p / 2), 1) * g_data

        def inputs_conversion(feed_dict):
            # Convert gradu to a numpy array of the right shape.
            gradu = feed_dict[g_data]
            gradu_np = np.reshape(gradu.vector()[:], shape)
            feed_dict[g_data] = gradu_np
            return feed_dict

        def adj_inputs_conversion(adj_inputs):
            adj_inputs[0] = adj_inputs[0][:].reshape(shape)
            return adj_inputs

        def outputs_conversion(outputs):
            f = Function(V_vec)
            f.vector()[:] = outputs[0].flatten()
            return [f]

        def adj_outputs_conversion(adj_outputs):
            adj_gradu = Function(V_vec)
            adj_gradu.vector()[:] = adj_outputs[g_data].flatten()
            adj_outputs[g_data] = adj_gradu.vector()
            return adj_outputs

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(
                g_output,
                {g_data: gradu, g_p: p},
                inputs_conversion=inputs_conversion,
                outputs_conversion=outputs_conversion,
                adj_inputs_conversion=adj_inputs_conversion,
                adj_outputs_conversion=adj_outputs_conversion,
            )

            if use_two_blocks:
                output = run_tensorflow_graph(
                    g_output,
                    {g_data: output, g_p: p},
                    inputs_conversion=inputs_conversion,
                    outputs_conversion=outputs_conversion,
                    adj_inputs_conversion=adj_inputs_conversion,
                    adj_outputs_conversion=adj_outputs_conversion,
                )

        rf_gradu = ReducedFunction(output, Control(gradu))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(gradu)])

    def numpy_func(p, data):
        scale = (np.sum(data * data, axis=1) + 1e-12)[:, None]
        return ((scale ** (p / 2)) * data), scale

    def numpy_der(p, data):
        correct_jac_p = correct_output * 0.5 * np.log(scale)

        correct_jac_gradu_small = np.matmul(
            (p * (scale ** (p / 2 - 1)) * data)[:, :, None], data[:, None, :]
        )
        diag = np.arange(2)
        correct_jac_gradu_small[:, diag, diag] += scale ** (p / 2)

        # Get correct_jac_gradu into the right shape.
        d = np.arange(n)
        correct_jac_gradu = np.zeros((n, 2, n, 2))
        correct_jac_gradu[d, :, d, :] = correct_jac_gradu_small

        return correct_jac_p, correct_jac_gradu

    if use_two_blocks:
        p2 = p * p + 2 * p
        correct_output, scale = numpy_func(p2, data)
        correct_jac_p, correct_jac_gradu = numpy_der(p2, data)
        correct_jac_p *= 2 * p + 2
    else:
        correct_output, scale = numpy_func(p, data)
        correct_jac_p, correct_jac_gradu = numpy_der(p, data)

    assert_allclose(output.vector()[:], correct_output.flatten())

    # Test rf_p
    jac = rf_p.jac_matrix()
    assert isinstance(jac, np.ndarray)
    assert_allclose(jac.flatten(), correct_jac_p.flatten())
    assert jac.shape == (
        n,
        2,
    )

    # Test rf_gradu
    jac = rf_gradu.jac_matrix()
    assert isinstance(jac, np.ndarray)
    assert_allclose(jac.flatten(), correct_jac_gradu.flatten())
    assert jac.shape == (n, 2, n, 2)

    # Test rf_both
    jacs = rf_both.jac_matrix()
    assert isinstance(jacs, (list, tuple))
    assert len(jacs) == 2
    assert_allclose(jacs[0], correct_jac_p)
    assert_allclose(jacs[1], correct_jac_gradu)

    # Test Jacobian action.
    h_p = AdjFloat(1e-3)
    h_gradu = project(Expression(("x[0]+x[1]", "x[1]*x[0]"), degree=1) * 1e-1, V_vec)
    h_gradu_np = h_gradu.vector()[:].reshape(shape)
    h_both = [h_p, h_gradu_np]

    jac_action = rf_p.jac_action(h_p)
    correct_jac_action_p = np.tensordot(correct_jac_p, h_p, axes=0)
    assert_allclose(jac_action.vector()[:], correct_jac_action_p.flatten())

    jac_action = rf_gradu.jac_action(h_gradu_np)
    correct_jac_action_gradu = np.tensordot(
        correct_jac_gradu, h_gradu_np, axes=len(h_gradu_np.shape)
    )
    assert_allclose(jac_action.vector()[:], correct_jac_action_gradu.flatten())

    jac_action = rf_both.jac_action(h_both)
    correct_jac_action_both = correct_jac_action_p + correct_jac_action_gradu
    assert_allclose(jac_action.vector()[:], correct_jac_action_both.flatten())

    sess.close()


if __name__ == "__main__":
    test_scalar_function_input()
    test_vector_function_input()
    test_scalar_function_jacobian(False)
    test_scalar_function_jacobian(True)
    test_vector_function_jacobian(False)
    test_vector_function_jacobian(True)
