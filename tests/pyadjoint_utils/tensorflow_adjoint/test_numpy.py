import pytest

tf = pytest.importorskip("tensorflow.compat.v1")

import numpy as np
from numpy_adjoint import *
from pyadjoint_utils.numpy_adjoint import *

try:
    tf.disable_eager_execution()
    from pyadjoint_utils.tensorflow_adjoint import *

    tf.keras.backend.set_floatx("float64")
except:
    pass

from pyadjoint import *
from pyadjoint.overloaded_type import create_overloaded_object
from pyadjoint_utils import *

from numpy.testing import assert_allclose


def test_scalar_function_input():
    n = 20

    np_data = np.arange(n) + 1
    data = create_overloaded_object(np_data)
    p = AdjFloat(2)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_output = g_data ** g_p

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(g_output, {g_data: data, g_p: p})
        J = output[5]

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(data)])

        Jhat_data = ReducedFunctional(J, Control(data))
        Jhat_p = ReducedFunctional(J, Control(p))
        Jhat_both = ReducedFunctional(J, [Control(p), Control(data)])

    with stop_annotating():
        correct_output = data ** p
        assert_allclose(output, correct_output)

        ### Test data input.
        data_rev = np_data[::-1]
        correct_output = data_rev ** p
        assert_allclose(rf_data(data_rev), correct_output)

        h_data = create_overloaded_object(np.random.rand(*data.shape))
        assert taylor_test(Jhat_data, data, h_data) > 1.9

        ### Test p input.
        rf_data(data)  # Set data's block variable back to original data.
        correct_output = data ** 3
        assert_allclose(rf_p(3), correct_output)

        h_p = AdjFloat(1e-3)
        assert taylor_test(Jhat_p, p, h_p) > 1.9

        ### Test data and p inputs.
        correct_output = data_rev ** 4
        assert_allclose(rf_both([4, data_rev]), correct_output)

        h_both = [h_p, h_data]
        assert taylor_test(Jhat_both, [p, data], h_both) > 1.9

        sess.close()


def test_vector_function_input():
    n = 40
    shape = (-1, 2)

    np_data = np.reshape(np.arange(n) + 1, shape)
    data = create_overloaded_object(np_data)
    p = AdjFloat(0.5)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_scale = g_data * g_data
            with tf.name_scope("reduce_sum"):
                g_scale = tf.reduce_sum(g_scale, axis=1) + 1e-12

            g_output = tf.expand_dims(g_scale ** (g_p / 2), 1) * g_data

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(
                g_output, {g_data: data, g_p: p}, pointwise=(g_data,)
            )
        J = output[5, 1]

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(data)])

        Jhat_data = ReducedFunctional(J, Control(data))
        Jhat_p = ReducedFunctional(J, Control(p))
        Jhat_both = ReducedFunctional(J, [Control(p), Control(data)])

    def numpy_func(p, data):
        scale = np.sum(data * data, axis=1) + 1e-12
        return (scale ** (p / 2))[:, None] * data

    correct_output = numpy_func(p, data)
    assert_allclose(output, correct_output)

    ### Test data input.
    data_rev = np_data[:, ::-1]
    correct_output = numpy_func(p, data_rev)
    assert_allclose(rf_data(data_rev), correct_output)

    h_data = create_overloaded_object(np.random.rand(*data.shape))
    assert taylor_test(Jhat_data, data, h_data) > 1.9

    ### Test p input.
    rf_data(data)  # Set data's block variable back to original u.
    new_p = 1
    correct_output = numpy_func(new_p, data)
    assert_allclose(rf_p(new_p), correct_output)

    h_p = AdjFloat(1e-3)
    assert taylor_test(Jhat_p, p, h_p) > 1.9

    ### Test u and p inputs.
    new_p = 2
    correct_output = numpy_func(new_p, data_rev)
    assert_allclose(rf_both([new_p, data_rev]), correct_output)

    assert taylor_test(Jhat_both, [p, data], [h_p, h_data]) > 1.9

    ## Test TLM.
    assert taylor_test(rf_both, [p, data], [h_p, h_data]) > 1.9

    sess.close()


@pytest.mark.parametrize("use_two_blocks", [False, True])
def test_scalar_function_jacobian(use_two_blocks):
    n = 41

    np_data = np.arange(n) + 1
    data = create_overloaded_object(np_data)
    p = AdjFloat(2)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_output = g_data ** g_p

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(g_output, {g_data: data, g_p: p})

            if use_two_blocks:
                output = run_tensorflow_graph(g_output, {g_data: output, g_p: p})

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(data)])

    if use_two_blocks:
        p2 = p * p
        correct_output = data ** p2
        correct_jac_p = (data ** p2) * np.log(data) * 2 * p
        correct_jac_u = np.diag(data ** (p2 - 1) * p2)
    else:
        correct_output = data ** p
        correct_jac_p = data ** p * np.log(data)
        correct_jac_u = np.diag(data ** (p - 1) * p)

    assert_allclose(output, correct_output)

    jac = rf_p.jac_matrix()
    assert isinstance(jac, np.ndarray)
    assert_allclose(jac.flatten(), correct_jac_p.flatten())
    assert jac.shape == (n,)

    jac = rf_data.jac_matrix()
    assert isinstance(jac, np.ndarray)
    assert_allclose(jac.flatten(), correct_jac_u.flatten())
    assert jac.shape == (n, n)

    jacs = rf_both.jac_matrix()
    assert isinstance(jacs, (list, tuple))
    assert len(jacs) == 2
    assert_allclose(jacs[0], correct_jac_p)
    assert_allclose(jacs[1], correct_jac_u)

    sess.close()


def test_vector_function_jacobian():
    n_vec = 82
    n = n_vec // 2
    shape = (-1, 2)

    np_data = np.reshape(np.arange(n_vec) + 1, shape)
    data = create_overloaded_object(np_data)
    p = AdjFloat(0.5)

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.constant(p, dtype=tf.float64, name="p")

            g_scale = g_data * g_data
            with tf.name_scope("reduce_sum"):
                g_scale = tf.reduce_sum(g_scale, axis=1) + 1e-12

            g_output = tf.expand_dims(g_scale ** (g_p / 2), 1) * g_data

        # Run graph.
        sess = tf.Session(graph=g)
        with sess.as_default():
            output = run_tensorflow_graph(
                g_output, {g_data: data, g_p: p}, pointwise=(g_data,)
            )

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, Control(p))
        rf_both = ReducedFunction(output, [Control(p), Control(data)])

    def numpy_func(p, data):
        scale = (np.sum(data * data, axis=1) + 1e-12)[:, None]
        return ((scale ** (p / 2)) * data), scale

    correct_output, scale = numpy_func(p, data)
    assert_allclose(output, correct_output)

    correct_jac_p = correct_output * 0.5 * np.log(scale)

    correct_jac_data_small = np.matmul(
        (p * (scale ** (p / 2 - 1)) * data)[:, :, None], data[:, None, :]
    )
    diag = np.arange(2)
    correct_jac_data_small[:, diag, diag] += scale ** (p / 2)

    # Test rf_p
    jac = rf_p.jac_matrix()
    assert isinstance(jac, np.ndarray)
    assert_allclose(jac.flatten(), correct_jac_p.flatten())
    assert jac.shape == (n, 2)

    # Test batch Jacobian
    jac = rf_data.jac_matrix()
    assert isinstance(jac, np.ndarray)
    assert_allclose(jac, correct_jac_data_small)
    assert jac.shape == (n, 2, 2)

    # Test rf_both
    jacs = rf_both.jac_matrix()
    assert isinstance(jacs, (list, tuple))
    assert len(jacs) == 2
    assert_allclose(jacs[0], correct_jac_p)
    assert_allclose(jacs[1], correct_jac_data_small)

    sess.close()


def test_tensorflow_variables():
    data = AdjFloat(3)
    p = create_overloaded_object(np.array(2))

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.get_variable(
                shape=(), initializer=tf.zeros_initializer(), dtype=tf.float64, name="p"
            )

            g_output = g_data ** g_p

            sess = tf.Session()
            init = tf.global_variables_initializer()
            sess.run(init)

            feed_dict = get_params_feed_dict(sess)

        feed_dict[g_p] = p

        params = [o for i, o in feed_dict.items()]
        params_controls = [Control(p) for p in params]

        feed_dict[g_data] = data

        # Run graph.
        with sess.as_default():
            output = run_tensorflow_graph(g_output, feed_dict)

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, params_controls)
        rf_both = ReducedFunction(output, [*params_controls, Control(data)])

        J = (output - 128) ** 2

        Jhat_data = ReducedFunctional(J, Control(data))
        Jhat_p = ReducedFunctional(J, params_controls)
        Jhat_both = ReducedFunctional(J, [*params_controls, Control(data)])

    assert output == 9

    ### Test data input.
    # Run graph using taped computation.
    assert rf_data(5) == 25

    # Do taylor test.
    h = AdjFloat(1.0)
    assert taylor_test(Jhat_data, data, h) > 1.9

    ### Test p input.
    # Run graph using taped computation.
    rf_data(2)  # Set data's block variable back to 2.
    assert rf_p([3]) == 8

    # Do taylor test.
    h = create_overloaded_object(np.array(1.0))
    assert taylor_test(Jhat_p, params, h) > 1.9

    ### Test data and p inputs.
    # Run graph using taped computation.
    assert rf_both([4, 3]) == 81

    # Do taylor test.
    h = [create_overloaded_object(np.array(1.0)), AdjFloat(2.0)]
    assert taylor_test(Jhat_both, [*params, data], h) > 1.9

    # Test optimize.
    rf_data(2)  # Set data's block variable back to 2.
    p_opt = minimize(Jhat_p)
    assert_allclose(p_opt, [7])

    sess.close()


def test_tensorflow_variables_array():
    data = AdjFloat(3)
    p = create_overloaded_object(np.array([2]))

    with push_tape():
        # Build graph.
        g = tf.Graph()
        with g.as_default():
            g_data = tf.placeholder(dtype=tf.float64, name="data")
            g_p = tf.get_variable(
                shape=(1),
                initializer=tf.zeros_initializer(),
                dtype=tf.float64,
                name="p",
            )

            g_output = g_data ** g_p
            g_output = tf.reshape(g_output, ())

            sess = tf.Session()
            init = tf.global_variables_initializer()
            sess.run(init)

            feed_dict = get_params_feed_dict(sess)

        feed_dict[g_p] = p

        params = [o for i, o in feed_dict.items()]
        params_controls = [Control(p) for p in params]

        feed_dict[g_data] = data

        # Run graph.
        with sess.as_default():
            output = run_tensorflow_graph(g_output, feed_dict)

        rf_data = ReducedFunction(output, Control(data))
        rf_p = ReducedFunction(output, params_controls)
        rf_both = ReducedFunction(output, [*params_controls, Control(data)])

        J = (output - 128) ** 2

        Jhat_data = ReducedFunctional(J, Control(data))
        Jhat_p = ReducedFunctional(J, params_controls)
        Jhat_both = ReducedFunctional(J, [*params_controls, Control(data)])

    assert output == 9

    ### Test data input.
    # Run graph using taped computation.
    assert rf_data(5) == 25
    assert Jhat_data(4) == (16 - 128) ** 2

    # Do taylor test.
    h = AdjFloat(1.0)
    assert taylor_test(Jhat_data, data, h) > 1.9

    ### Test p input.
    # Run graph using taped computation.
    rf_data(2)  # Set data's block variable back to 2.
    assert rf_p([3]) == 8

    # Do taylor test.
    h = create_overloaded_object(np.array([1.0]))
    assert taylor_test(Jhat_p, params, h) > 1.9

    ### Test data and p inputs.
    # Run graph using taped computation.
    assert rf_both([4, 3]) == 81

    # Do taylor test.
    h = [create_overloaded_object(np.array([1.0])), AdjFloat(2.0)]
    assert taylor_test(Jhat_both, [*params, data], h) > 1.9

    # Test optimize.
    rf_data(2)  # Set data's block variable back to 2.
    p_opt = minimize(Jhat_p)
    assert_allclose(p_opt, [7])

    sess.close()


def test_tensorflow_network():
    from tensorflow.keras import layers

    np.random.seed(523)

    n = 20
    n_vec = 2 * n
    shape = (-1, 2)
    obs_index = (5, 0)

    np_data = (np.arange(n) + 1).reshape(shape)
    data = create_overloaded_object(np_data)

    g = tf.Graph()
    with g.as_default():
        g_data = tf.placeholder(dtype=tf.float64, shape=(None, 2), name="data")

        weight_init = tf.keras.initializers.RandomUniform(
            -0.3, 0.3, dtype=tf.dtypes.float64, seed=347
        )
        hidden_layer_kwargs = {
            "activation": "tanh",
            "kernel_initializer": weight_init,
            "bias_initializer": weight_init,
        }
        layer_sizes = [4, 3, 2]
        x = g_data
        for num in layer_sizes:
            x = layers.Dense(num, **hidden_layer_kwargs)(x)

        output_layer_kwargs = {
            "activation": "linear",
            "kernel_initializer": weight_init,
            "bias_initializer": weight_init,
        }
        g_output = layers.Dense(1, **output_layer_kwargs)(x)

        init = tf.global_variables_initializer()

        sess = tf.Session()
        sess.run(init)

        feed_dict = get_params_feed_dict(sess)
        params_keys = [g_p for g_p in feed_dict]
        params = [p for g_p, p in feed_dict.items()]
        params_controls = [Control(p) for p in params]

        h_data = create_overloaded_object(np.random.rand(*data.shape))
        h_p = [create_overloaded_object(np.random.rand(*p.shape)) for p in params]
        h_both = [*h_p, h_data]

        both = [*params, data]

        feed_dict[g_data] = data

    with push_tape():
        # Run graph.
        with sess.as_default():
            output = run_tensorflow_graph(g_output, feed_dict)
        J = output[obs_index]

        data_control = Control(data)

        rf_data = ReducedFunction(output, data_control)
        rf_p = ReducedFunction(output, params_controls)
        rf_both = ReducedFunction(output, [*params_controls, data_control])

        Jhat_data = ReducedFunctional(J, data_control)
        Jhat_p = ReducedFunctional(J, params_controls)
        Jhat_both = ReducedFunctional(J, [*params_controls, data_control])

    with stop_annotating():

        # Test adjoint derivative.

        assert taylor_test(Jhat_data, data, h_data) > 1.9
        assert taylor_test(Jhat_p, params, h_p) > 1.9
        assert taylor_test(Jhat_both, both, h_both) > 1.9

    with push_tape():
        # Run graph.
        with sess.as_default():
            output = run_tensorflow_graph(g_output, feed_dict)

        rf_data = ReducedFunction(output, data_control)
        rf_p = ReducedFunction(output, params_controls)
        rf_both = ReducedFunction(output, [*params_controls, data_control])

    with stop_annotating():

        assert taylor_test(rf_data, data, h_data) > 1.9
        assert taylor_test(rf_p, params, h_p) > 1.9
        assert taylor_test(rf_both, both, h_both) > 1.9

        v = create_overloaded_object(np.random.rand(*output.shape))
        assert taylor_test(rf_data, data, h_data, v=v) > 1.9
        assert taylor_test(rf_p, params, h_p, v=v) > 1.9
        assert taylor_test(rf_both, both, h_both, v=v) > 1.9

    sess.close()


if __name__ == "__main__":
    test_scalar_function_input()
    test_vector_function_input()
    test_scalar_function_jacobian(False)
    test_scalar_function_jacobian(True)
    test_vector_function_jacobian()
    test_tensorflow_variables()
    test_tensorflow_variables_array()
    test_tensorflow_network()
