from pyadjoint import *
from pyadjoint_utils import *
from pyadjoint_utils.tape_block import record_tape_block
from numpy.testing import assert_approx_equal, assert_allclose


def test_tape_block():
    def test_compute(a):
        with record_tape_block():
            b_0 = a[0] * a[1]
            b_1 = a[2] * b_0
            b_2 = -a[2]
            return (b_0, b_1, b_2)

    # Run some test computations on a separate tape.
    x = (AdjFloat(3.0), AdjFloat(5.0), AdjFloat(7.0))
    y = test_compute(x)

    # Test recompute and adjoint for single tape block.
    J = ReducedFunction(y, tuple(Control(xi) for xi in x))
    m = tuple(AdjFloat(f) for f in (2, -3, -4))
    h = [AdjFloat(1e-0) for c in J.controls]

    new_y = tuple(J(m))
    assert new_y != y
    assert new_y == (-6, 24, 4)
    assert taylor_test(J, m, h) > 1.9

    # Run some more computations to test two TapeBlocks.
    z = (y[0] * y[2], AdjFloat(2.0), y[2])
    w = test_compute(z)

    # Test recompute and adjoint for connected tape blocks.
    J = ReducedFunction(w, tuple(Control(xi) for xi in x) + (Control(z[1]),))
    m = tuple(AdjFloat(f) for f in (2, -3, -4, 0.5))
    h = [AdjFloat(1e-0) for c in J.controls]

    new_w = tuple(J(m))
    assert new_w != w
    assert new_w == (-12, -48, -4)
    assert taylor_test(J, m, h) > 1.9


if __name__ == "__main__":
    test_tape_block()
