from pyadjoint_utils import array
from pyadjoint_utils import overload_jax
from pyadjoint_utils import *
import pyadjoint_utils
import pyadjoint
from pyadjoint.overloaded_type import create_overloaded_object
from pyadjoint.enlisting import Enlist
from crikit.cr import *
import jax.numpy as np
import numpy as onp
import jax
from jax import jacrev
from functools import partial
import pytest


def test_univariate():

    arr = array(1.0)

    @overload_jax
    def f(x):
        return x ** 4

    y = f(arr)

    rf = ReducedFunction(y, Control(arr))
    inputs = [arr]
    outputs = [y]
    h = (array(1.0),)  # array(onp.random.randn(*a.shape)) for a in inputs)
    v = (array(1.0),)  # tuple(array(onp.random.randn(*a.shape)) for a in outputs)
    res = taylor_test(rf, inputs, h)  # ,v=v)
    assert res >= 1.9
    # rate_keys = ["R0", "R1",]# "R2"]
    # results = taylor_to_dict(rf,inputs, h, v=v)
    # for order, k in enumerate(rate_keys):
    #    assert min(results[k]["Rate"]) > order + 0.9


def test_single_input(taylor_tol=0.1):
    # asserts that the convergence rate from the Taylor test is
    # at least 2 - taylor_tol
    arr = array(np.array([2.0, 2.0], dtype=np.float64))
    other = onp.array(arr)
    ###print(f"numpy array from jax_adjoint.ndarray: {other}")
    @overload_jax
    def f(x):
        return np.array([x[0] * x[0], x[1] * x[1]])

    @overload_jax
    def g(x):
        return np.sum(x)

    assert np.allclose(f(arr).value, np.array([4.0, 4.0]))

    y = f(arr)
    out = g(y)
    gf = ReducedFunction(out, Control(arr))
    inputs = [arr]
    outputs = [gf(arr)]
    h = tuple(1e-3 * array(onp.random.randn(*a.shape)) for a in inputs)
    v = tuple(array(onp.random.randn(*a.shape)) for a in outputs)
    res = taylor_test(gf, inputs, h)
    assert np.allclose(
        jacrev(f)(arr).value, np.array([[4.0, 0.0], [0.0, 4.0]]), 1.0e-3
    ), f"{jacrev(f)(arr)} != {array(np.array([[4.0,0.0],[0.0,4.0]]))}"
    assert (
        res >= 2.0 - taylor_tol
    ), f"Smallest Taylor test convergence rate {res} is less than 2 with tolerance {taylor_tol}!"

    # rate_keys = ["R0", "R1", "R2"]
    # results = taylor_to_dict(gf,inputs, h, v=v)
    # for order, k in enumerate(rate_keys):
    #    assert min(results[k]["Rate"]) > order + 0.9


@partial(overload_jax, argnums=(0, 1), checkpoint=True)
def one_output(A, B):
    return np.exp(np.trace(A @ B))


@partial(overload_jax, argnums=(0, 1), checkpoint=True)
def two_outputs(A, B):
    return np.exp(np.trace(A @ B)), np.exp(np.trace(A @ A @ B @ B))


@partial(overload_jax, argnums=(0, 1), checkpoint=True)
def two_matrix_outputs(A, B):
    return np.exp(np.trace(A @ B)) * A, np.exp(np.trace(A @ A @ B @ B)) * B


@partial(overload_jax, argnums=(0, 1), checkpoint=True, nojit=True, concrete=True)
def one_output_unjittable(A, B):
    if A[0, 0] < 0.5:
        return np.exp(np.trace(A @ B))
    return -3 * np.exp(np.trace(A @ B))


@partial(overload_jax, argnums=(1, 2), checkpoint=False, nojit=True, concrete=True)
@partial(jax.jit, static_argnums=(0,))
def one_output_static_arg(k, A, B):
    if k < 0.5:
        return np.exp(np.trace(A @ B))
    return -3 * np.exp(np.trace(A @ B))


one_output_static_arg = partial(one_output_static_arg, 0.7)


class OneOutputClass:
    x: int = 3

    @partial(overload_jax, argnums=(1, 2), nojit=True)
    @partial(jax.jit, static_argnums=(0,))
    def __call__(self, A, B):
        return np.exp(self.x * np.trace(A @ B))


@partial(
    overload_jax,
    argnums=(0, 1),
    checkpoint=True,
    pointwise=(True, False),
    out_pointwise=(False, True, True, False),
)
@partial(jax.vmap, in_axes=(0, None), out_axes=(None, 0, 0, None))
def four_outputs_pointwise(v, B):
    one = np.exp(np.trace(B)) * B
    two = np.outer(B @ v, v)
    three = np.dot(v, B @ v) * two
    four = np.exp(np.trace(B @ B)) * B
    return one, two, three, four


@pytest.mark.parametrize(
    "func",
    [
        one_output,
        two_outputs,
        two_matrix_outputs,
        one_output_unjittable,
        one_output_static_arg,
        OneOutputClass(),
        four_outputs_pointwise,
    ],
)
def test_two_input(func, **kwargs):
    X = array(onp.random.uniform(0.0, 1.0, (3, 3)))
    Y = array(onp.random.uniform(0.0, 1.0, (3, 3)))
    inputs = (X, Y)
    controls = tuple(Control(x) for x in inputs)
    outputs = Enlist(func(*inputs))
    rf = ReducedFunction(outputs, controls)

    # Test Jacobian.
    J = rf.jac_matrix()
    jac_func = jax.jacfwd(func, argnums=(0, 1))
    jac_tuple = jac_func(*inputs)
    if not outputs.listed:
        jac_tuple = (jac_tuple,)
    try:
        gr = np.array(tuple(x for x in J))
        jax_jac = [np.stack([x.value for x in j]) for j in jac_tuple]
        assert np.allclose(gr, jax_jac)
    except ValueError:
        # The Jacobians are of different shapes and can't be stacked, so compare each individually.
        assert len(J) == len(jac_tuple)
        for i, (Ji_mine, Ji_true) in enumerate(zip(J, jac_tuple)):
            for j, (Jij_mine, Jij_true) in enumerate(zip(Ji_mine, Ji_true)):
                truth = Jij_true.value
                if truth.shape != Jij_mine.shape:
                    # If both input j and output i are pointwise, there will be an extra axis to collapse.
                    truth = np.sum(truth, axis=len(outputs[i].shape))
                onp.testing.assert_allclose(Jij_mine, truth)

    # Visualize tape.
    dotfile = kwargs.get("visualise_tape", None)
    if dotfile:
        get_working_tape().visualise_dot("two_input.dot")

    # Test adjoint.
    h = tuple(1e-3 * array(onp.random.randn(*a.shape)) for a in inputs)
    v = tuple(array(onp.random.randn(*a.shape)) for a in outputs)
    tol = kwargs.get("taylor_tol", 0.1)
    res = taylor_test(rf, inputs, h, v=v)
    assert (
        res >= 2.0 - tol
    ), f"Taylor test minimum convergence rate {res} is less than 2!"

    # Test TLM.
    tol = kwargs.get("taylor_tol", 0.1)
    res = taylor_test(rf, inputs, h, v=v)
    assert (
        res >= 2.0 - tol
    ), f"Taylor test minimum convergence rate {res} is less than 2!"

    # rate_keys = ["R0", "R1", "R2"]
    # results = taylor_to_dict(rf,inputs, h, v=v)
    # for order, k in enumerate(rate_keys):
    #    assert min(results[k]["Rate"]) > order + 0.9
