from crikit.fe import *
from crikit.fe_adjoint import *
from crikit import AdditiveRandomFunction
import pytest


def test_additive_random(distribution="normal", params={"mu": 0, "std": 1}):
    mesh = UnitSquareMesh(2, 2)
    V = FunctionSpace(mesh, "CG", 1)
    expr = Expression("x[1]*x[0]", degree=1)
    c = Constant(1.4)
    u = project(c * expr, V)
    observer = AdditiveRandomFunction(V, distribution=distribution, **params)
    v = observer(u)
    v *= c
    rf = ReducedFunctional(assemble(inner(v, v) * dx), Control(c))
    assert taylor_test(rf, c, Constant(0.1)) >= 1.9
