from crikit import (
    CR,
    get_invariant_functions,
    get_invariant_descriptions,
    InvariantInfo,
    TensorType,
    LeviCivitaType,
    eps_ij,
    near,
    eps_ijk,
    symm,
    cr_function_shape,
)
from crikit.invariants.utils import _3d_rotation_matrix
from pyadjoint_utils import overload_jax
from pyadjoint_utils.jax_adjoint import array
import pyadjoint_utils
import pyadjoint
from pyadjoint import *
from pyadjoint_utils import *
import jax
from jax import numpy as np
from functools import partial
from jax import random
import numpy as onp


def _2d_rotation_matrix(theta):
    return np.array([[np.cos(theta), np.sin(theta)], [-np.sin(theta), np.cos(theta)]])


def rotation_rank_2_action(x, g):
    return g.T @ x @ g


def test_description():
    inputs = (
        TensorType.make_symmetric(2, 3, "epsilon"),
        TensorType.make_symmetric(2, 3),
        TensorType.make_vector(3, "k"),
        TensorType.make_symmetric(2, 3),
        TensorType.make_vector(3),
        TensorType.make_scalar("P"),
    )
    output = TensorType.make_symmetric(2, 3)
    info = InvariantInfo(3, inputs, output)
    print(get_invariant_descriptions(info, suppress_warning_print=True))


def test_3d_two_rank_two():
    inputs = (TensorType.make_symmetric(2, 3), TensorType.make_symmetric(2, 3))
    info = InvariantInfo(3, inputs, TensorType.make_symmetric(2, 3))
    print(get_invariant_descriptions(info, suppress_warning_print=False))
    s_inv, f_inv = get_invariant_functions(info, fail_on_warning=False)

    one = np.diag(np.array([2, 3, 5]))
    two = np.diag(np.array([7, 11, 13]))

    scalars = s_inv(one, two)

    def get_scalars(A, B):
        A2 = A @ A
        B2 = B @ B
        a = [np.trace(A), np.trace(A2), np.trace(A @ A2)]
        b = [np.trace(B), np.trace(B2), np.trace(B @ B2)]
        ab = [np.trace(A @ B), np.trace(A2 @ B), np.trace(A @ B2), np.trace(A2 @ B2)]
        return np.array(a + b + ab)

    correct = get_scalars(one, two)

    # print('correct', 'invariants.py')
    for m, s in zip(correct, scalars):
        assert m == s


def test_2d_isotropic_one_rank_2():

    example_inputs = (np.eye(2),)
    example_output = np.eye(2)
    # one symmetric rank-2 input, one symmetric rank-2 output
    def cr_func(scalar_invts, p):
        return scalar_invts ** ((p - 2.0) / 2.0)

    p = array(np.array([2.5]))
    cr = CR.from_arrays(
        example_output,
        example_inputs,
        cr_func,
        param_argnums=(1,),
        params=(p,),
        vmap=False,
    )

    example_eps = array(symm(np.array([[3.0, 4.0], [5.0, 6.0]])))
    example_sigma = cr(example_eps)

    thetas = [0.1, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    sinvts = cr.scalar_invariants(example_eps.value)
    for th in thetas:
        rotated_input = rotation_rank_2_action(example_eps, _2d_rotation_matrix(th))
        rotated_output = cr(rotated_input)
        assert np.allclose(sinvts, cr.scalar_invariants(rotated_input.value))
        rotated_output = rotation_rank_2_action(
            rotated_output, _2d_rotation_matrix(-th)
        )
        assert np.allclose(rotated_output.value, example_sigma.value)


def test_2d_hemitropic_one_rank_2():

    example_inputs = (np.eye(2), eps_ij)
    example_output = np.eye(2)
    # one symmetric rank-2 input, one symmetric rank-2 output
    def cr_func(scalar_invts, p):
        return np.array([scalar_invts[0], scalar_invts[1], np.mean(scalar_invts)]) ** (
            (p - 2.0) / 2.0
        )

    p = array(np.array([2.5]))
    cr = CR.from_arrays(example_output, example_inputs, cr_func, (1,), (p,), vmap=False)

    example_eps = array(symm(np.array([[3.0, 4.0], [5.0, 6.0]])))
    example_sigma = cr(example_eps)

    thetas = [0.1, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    sinvts = cr.scalar_invariants(example_eps.value)
    for th in thetas:
        rotated_input = rotation_rank_2_action(example_eps, _2d_rotation_matrix(th))
        rotated_output = cr(rotated_input)
        assert np.allclose(sinvts, cr.scalar_invariants(rotated_input.value))
        rotated_output = rotation_rank_2_action(
            rotated_output, _2d_rotation_matrix(-th)
        )
        assert np.allclose(rotated_output.value, example_sigma.value)

    @partial(overload_jax, argnums=(1,))
    def of(x, p):
        cr.set_params((p,))
        return np.sum(cr(x))

    v = of(example_eps, p)

    rf = ReducedFunction(v, Control(p))

    h = tuple(1.0e-2 * array(onp.random.randn(*x.shape)) for x in (p,))
    p = array([2.8])
    res = pyadjoint_utils.taylor_test(rf, p, h)

    assert res >= 1.9


def test_3d_transverse_isotropy():

    example_inputs = (np.eye(3), np.ones((3,)))
    example_output = np.eye(3)

    def cr_func(scalar_invts, p):
        return ((scalar_invts + 1.0e-12) ** p)[0:8]

    p = array(np.array([2.5]))
    cr = CR.from_arrays(example_output, example_inputs, cr_func, (1,), (p,), vmap=False)

    example_eps = array(np.eye(3))
    example_v = array(np.array([1.0, 1.0, 1.0]) / np.sqrt(3))
    example_sigma = cr(example_eps, example_v)

    thetas = [-0.05, 0.1, 0.3, 0.5, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    sinvts = cr.scalar_invariants(example_eps.value, example_v.value)

    for th in thetas:
        rotmat = _3d_rotation_matrix(
            np.array([1.0, 1.0, 1.0]) / np.sqrt(3), th
        )  # rotation about the vector that is the structural tensor
        rotated_eps = rotmat @ example_eps @ rotmat.T
        rotated_v = rotmat @ example_v
        rotated_sigma = cr(rotated_eps, rotated_v)
        rsi = cr.scalar_invariants(rotated_eps.value, rotated_v.value)
        irotmat = _3d_rotation_matrix(example_v.value, -th)
        assert np.allclose(rsi, sinvts, rtol=1.0e-1), f"theta = {th}, {rsi} != {sinvts}"

    si, fi = cr._scalar_invt_func, cr._form_invt_func
    f = cr._f
    eval_invt_cr = cr._invariant_evaluator

    @partial(overload_jax, argnums=(0, 1, 2))
    def cr_fun(p, eps, v):
        sinvts = si(eps, v)
        finvts = fi(eps, v)
        phi_i = f(sinvts, p)
        return eval_invt_cr(phi_i, finvts)

    # ensure that everything is differentiable
    @partial(overload_jax, argnums=(2,))
    def cr_class_fun(eps, v, p):
        cr.set_params((p,))
        return cr(eps, v)

    sigma = cr_fun(p, example_eps, example_v)
    rf = ReducedFunction(sigma, [Control(p), Control(example_eps), Control(example_v)])
    ssigma = cr_class_fun(example_eps, example_v, p)
    rrf = ReducedFunction(ssigma, Control(p))
    hp = (1.0e-2 * array(onp.random.randn(*p.shape)),)
    h = tuple(
        1.0e-2 * array(onp.random.randn(*x.shape))
        for x in (p, example_sigma, example_v)
    )
    res = pyadjoint_utils.taylor_test(rrf, (p,), hp)
    assert res >= 1.9
    res = pyadjoint_utils.taylor_test(rf, (p, example_eps, example_v), h)
    assert res >= 1.9


def test_3d_isotropy_temperature():
    eps = array(np.eye(3))
    T = array(np.array([0.5]))
    ex_out = np.eye(3)
    n_scalar, n_phi = cr_function_shape(ex_out, (eps, T))

    def cr_func(scalar_invts, p):
        return np.mean(scalar_invts) ** ((p - 2.0) / 2.0) * np.ones((n_phi,))

    p = array(2.5)
    cr = CR.from_arrays(
        ex_out,
        (
            eps,
            T,
        ),
        cr_func,
        param_argnums=(1,),
        params=(p,),
        vmap=False,
    )
    thetas = [-0.05, 0.1, 0.3, 0.5, np.pi / 4, np.pi / 2, 3 * np.pi / 4]
    sinvts = cr.scalar_invariants(eps.value, T.value)
    ex_sigma = cr(eps, T)
    for th in thetas:
        ax = onp.random.randn(3)
        ax = ax / onp.linalg.norm(ax)
        rotmat = _3d_rotation_matrix(ax, th)
        rotated_eps = rotmat @ eps @ rotmat.T
        rsi = cr.scalar_invariants(rotated_eps.value, T.value)
        assert np.allclose(rsi, sinvts, rtol=1.0e-1), f"theta = {th}, {rsi} != {sinvts}"

    rf = ReducedFunction(ex_sigma, Control(p))
    h = tuple(1.0e-1 * array(onp.random.randn(*x.shape)) for x in (p,))
    assert taylor_test(rf, (p,), h) >= 1.9


if __name__ == "__main__":
    test_3d_two_rank_two()
    test_description()
    test_2d_isotropic_one_rank_2()
    test_2d_hemitropic_one_rank_2()
    test_3d_transverse_isotropy()
    test_3d_isotropy_temperature()
