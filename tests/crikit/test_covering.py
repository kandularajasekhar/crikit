import pytest

from crikit.covering import (
    Covering,
    get_composite_cr,
    set_default_covering_params,
    reset_default_covering_params,
    Numpy_UFLFunctionSpace_Covering,
    UFLFunctionSpace_UFLExpr_Covering,
    To_UFLFunctionSpace,
    UFLExprSpace_To_Numpy,
    Numpy_To_UFLFunctionSpace,
)
from crikit.cr.numpy import CR_P_LaplacianNumpy
from crikit.cr.ufl import CR_P_Laplacian, UFLFunctionSpace
from crikit.cr.space_builders import DirectSum
from crikit.cr.map_builders import CompositePointMap
from crikit.fe import *
from crikit.fe_adjoint import *
from crikit.projection import project
from pyadjoint_utils import AdjFloat

# Test that the path ufl_expr -> cr_ufl -> ufl_expr -> ufl_function_space will
#    give the same result as ufl_expr -> quad_space -> numpy -> cr_numpy ->
#    numpy -> quad_space -> ufl_function_space.
@pytest.mark.parametrize("input_u", [False, True])
def test_covering_with_gradu(input_u):
    mesh = UnitSquareMesh(3, 3)
    V = FunctionSpace(mesh, "P", 1)
    V_vec = FunctionSpace(mesh, "RTE", 1)

    if crikit_fe_backend == "firedrake":
        x = SpatialCoordinate(V.mesh())
        v = interpolate(x[0] * x[0] + x[1] * x[1], V)
    else:
        v = interpolate(Expression("x[0]*x[0] + x[1]*x[1]", degree=2), V)

    g = grad(v)

    if input_u:
        inputs = (v, g)
    else:
        inputs = g

    p_np = AdjFloat(2.5)
    p_ufl = Constant(p_np)

    cr_ufl = CR_P_Laplacian(p_ufl, input_u=input_u)
    cr_np = CR_P_LaplacianNumpy(p_np, input_u=input_u)

    out_function_space = UFLFunctionSpace(V_vec)

    quad_params = {"quadrature_degree": 2}
    domain = mesh.ufl_domain()

    if not input_u:
        # Test 1: explicitly create point maps that do the desired mapping.

        source_expr_to_np = UFLExprSpace_To_Numpy(
            cr_ufl.source, cr_np.source, domain=domain, quad_params=quad_params
        )
        target_np_to_mesh = Numpy_To_UFLFunctionSpace(
            cr_np.target, out_function_space, quad_params=quad_params
        )

        target_expr_to_mesh = To_UFLFunctionSpace(cr_ufl.target, out_function_space)

        cr_np_composite = CompositePointMap(source_expr_to_np, cr_np, target_np_to_mesh)
        cr_ufl_composite = CompositePointMap(cr_ufl, target_expr_to_mesh)

        sigma_ufl = cr_ufl_composite(g)
        sigma_np = cr_np_composite(g)

        assert errornorm(sigma_ufl, sigma_np) < 1e-7

        # Test 2: use covering maps to do the desired mapping.

        source_expr_covering = Numpy_UFLFunctionSpace_Covering(
            cr_ufl.source, domain=domain, quad_params=quad_params
        )
        target_mesh_covering = Numpy_UFLFunctionSpace_Covering(
            out_function_space, domain=domain, quad_params=quad_params
        )
        source_expr_to_np = source_expr_covering.section_map()
        target_np_to_mesh = target_mesh_covering.covering_map()

        target_expr_covering = UFLFunctionSpace_UFLExpr_Covering(
            cr_ufl.target, out_function_space
        )
        target_expr_to_mesh = target_expr_covering.section_map()

        cr_np_composite = CompositePointMap(source_expr_to_np, cr_np, target_np_to_mesh)
        cr_ufl_composite = CompositePointMap(cr_ufl, target_expr_to_mesh)

        sigma_ufl = cr_ufl_composite(g)
        sigma_np = cr_np_composite(g)

        assert errornorm(sigma_ufl, sigma_np) < 1e-7

    # Test 3: use the automatic covering lookup feature.

    # 3a. Pass the parameters to get_composite_cr().
    cr_np_composite = get_composite_cr(
        cr_ufl.source, cr_np, out_function_space, domain=domain, quad_params=quad_params
    )
    cr_ufl_composite = get_composite_cr(cr_ufl.source, cr_ufl, out_function_space)

    sigma_ufl = cr_ufl_composite(inputs)
    sigma_np = cr_np_composite(inputs)

    assert errornorm(sigma_ufl, sigma_np) < 1e-7

    with pytest.raises(ValueError):
        # 3b. This should fail because the domain wasn't specified.
        cr_np_composite = get_composite_cr(cr_ufl.source, cr_np, out_function_space)

    # 3c. Set the parameters using set_default_covering_params().
    set_default_covering_params(domain=domain, quad_params=quad_params)
    cr_np_composite = get_composite_cr(cr_ufl.source, cr_np, out_function_space)
    cr_ufl_composite = get_composite_cr(cr_ufl.source, cr_ufl, out_function_space)

    sigma_ufl = cr_ufl_composite(inputs)
    sigma_np = cr_np_composite(inputs)

    assert errornorm(sigma_ufl, sigma_np) < 1e-7

    reset_default_covering_params()
    with pytest.raises(ValueError):
        # 3d. This should fail because the domain was removed from the params.
        cr_np_composite = get_composite_cr(cr_ufl.source, cr_np, out_function_space)


if __name__ == "__main__":
    test_covering_with_gradu()
