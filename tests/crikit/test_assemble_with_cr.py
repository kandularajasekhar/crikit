from crikit.cr.types import PointMap
from crikit.cr.ufl import CR_P_Laplacian, CR_UFL_Expr, UFLExprSpace
from crikit.fe import *
from crikit.fe_adjoint import *
from crikit.cr import assemble_with_cr, JAXArrays
from pyadjoint import AdjFloat, ReducedFunctional
from pyadjoint.tape import get_working_tape
from pyadjoint_utils import *
from crikit.cr.autograd import point_map
from crikit.cr.map_builders import Callable, Parametric
from crikit.cr.space_builders import DirectSum
from pyadjoint_utils.numpy_adjoint import *
from pyadjoint.overloaded_type import create_overloaded_object
from ufl import derivative, replace

import autograd.numpy as anp
import jax
import jax.numpy as jnp
import numpy as onp
import pytest
from functools import partial
from numpy.testing import assert_allclose


@point_map(((-1,), (-1, 2), ()), (-1, 2), bare=True, pointwise=(True, True, False))
def cr_autograd_full(u, g, p):
    mu = anp.sum(g * g, axis=1) ** ((p - 2.0) / 2.0)
    mu = anp.reshape(mu, mu.shape + (1,))
    return mu * g


def make_cr_autograd(p_init):
    def constructor(x):
        return create_overloaded_object(anp.array(x))

    p = constructor(p_init)
    return Parametric(cr_autograd_full, 2, p), p, constructor


@partial(
    overload_jax,
    argnums=(0, 1, 2),
    checkpoint=True,
    nojit=True,
    pointwise=(True, True, False),
)
@partial(jax.vmap, in_axes=(0, 0, None), out_axes=0)
def jax_plap(u, g, p):
    mu = jnp.sum(g * g) ** ((p - 2.0) / 2)
    return mu * g


def make_cr_jax(p_init):
    def constructor(x):
        return create_overloaded_object(jnp.array(x))

    p = constructor(p_init)
    source = DirectSum(JAXArrays((-1,)), JAXArrays((-1, 2)), JAXArrays(()))
    target = JAXArrays((-1, 2))
    cr_jax_full = Callable(source, target, jax_plap, bare=True)
    return Parametric(cr_jax_full, 2, p), p, constructor


@pytest.mark.parametrize("cr_maker", [make_cr_autograd, make_cr_jax])
def test_vector(cr_maker):
    mesh = UnitSquareMesh(3, 3)
    V = FunctionSpace(mesh, "P", 1)
    if crikit_fe_backend != "firedrake":
        u = interpolate(Expression("x[0]*x[0] + x[1]*x[1]", degree=1), V)
    else:
        x = SpatialCoordinate(V.mesh())
        u = interpolate(x[0] * x[0] + x[1] * x[1], V)
    g = grad(u)

    pc = Constant(2.5)
    cr_ufl = CR_P_Laplacian(pc)
    cr_numpy, p, constructor = cr_maker(float(pc))

    sigma = cr_ufl.target.point()
    v = TestFunction(V)
    form = inner(sigma, grad(v)) * dx
    vec_ufl = Function(V)
    vec_numpy = Function(V)
    assemble_with_cr(form, cr_ufl, (u, grad(u)), sigma, tensor=vec_ufl)
    assemble_with_cr(form, cr_numpy, (u, grad(u)), sigma, tensor=vec_numpy)

    # Assert that the UFL-native and external calculations are the same
    assert errornorm(vec_ufl, vec_numpy) < 1.0e-7

    rf_ufl = ReducedFunction(vec_ufl, Control(pc))
    vec_ufl2 = rf_ufl(3.0)
    rf_numpy = ReducedFunction(vec_numpy, Control(p))
    vec_numpy2 = rf_numpy(3.0)

    # Assert that pyadjoint taped both functions identically
    assert errornorm(vec_ufl2, vec_numpy2) < 1.0e-7

    # Run taylor tests with adjoint.
    hc = Constant(1e-1)
    h = constructor(float(hc))

    v_vec = Function(V)
    v_vec.vector()[:] = onp.random.rand(len(v_vec.vector()))
    new_pc = Constant(3.0)
    new_p = constructor(float(new_pc))
    assert taylor_test(rf_ufl, new_pc, v=v_vec.vector(), h=hc) > 1.9
    assert taylor_test(rf_numpy, new_p, v=v_vec.vector(), h=h) > 1.9

    # Run taylor tests with tlm.
    assert taylor_test(rf_ufl, new_pc, h=hc) > 1.9
    assert taylor_test(rf_numpy, new_p, h=h) > 1.9

    # Similar tests but with u as the control.
    if crikit_fe_backend != "firedrake":
        u_new = interpolate(
            Expression(
                "2*x[0]*x[0]*x[1] + 2*x[1]*x[1]*x[0] + 0.01*(x[0] + x[1])", degree=1
            ),
            V,
        )
    else:
        x = SpatialCoordinate(V.mesh())
        u_new = interpolate(
            2 * x[0] * x[0] * x[1] + 2 * x[1] * x[1] * x[0] + 0.01 * (x[0] + x[1]), V
        )

    rf_ufl = ReducedFunction(vec_ufl, Control(u))
    vec_ufl2 = rf_ufl(u_new)
    rf_numpy = ReducedFunction(vec_numpy, Control(u))
    vec_numpy2 = rf_numpy(u_new)

    assert errornorm(vec_ufl2, vec_numpy2) < 1.0e-7

    # Run taylor tests with adjoint.
    h = Function(V)
    h.vector()[:] = 0.1 * onp.random.rand(V.dim())

    assert taylor_test(rf_ufl, u_new, v=v_vec.vector(), h=h) > 1.9
    assert taylor_test(rf_numpy, u_new, v=v_vec.vector(), h=h) > 1.9

    # Run taylor tests with tlm.
    assert taylor_test(rf_ufl, u_new, h=h) > 1.9
    assert taylor_test(rf_numpy, u_new, h=h) > 1.9

    # Test Jacobian.
    rfnp = ReducedFunctionNumPy(rf_numpy)
    J_numpy = rfnp.jac_matrix().array()

    u_hat = TrialFunction(V)
    cr_ufl.setParams(Constant(3.0))
    cr_form = replace(form, {sigma: cr_ufl((u, grad(u)))})
    der_ufl = derivative(replace(cr_form, {u: u_new}), u_new, u_hat)
    J_ufl = assemble(der_ufl).array()
    assert_allclose(J_numpy, J_ufl)


@point_map(((-1,), (-1, 2), ()), (-1,), bare=True, pointwise=(True, True, False))
def cr_numpy_energy(u, g, p):
    mag2 = anp.sum(g * g, axis=1) + 1e-12
    energy = mag2 ** (p / 2) / p
    return energy


def test_scalar():
    mesh = UnitSquareMesh(3, 3)
    V = FunctionSpace(mesh, "P", 1)
    if crikit_fe_backend != "firedrake":
        u = interpolate(Expression("x[0]*x[0] + x[1]*x[1]", degree=1), V)
    else:
        x = SpatialCoordinate(V.mesh())
        u = interpolate(x[0] * x[0] + x[1] * x[1], V)
    g = grad(u)

    p = create_overloaded_object(anp.array(2.5))
    pc = Constant(p)

    ufl_arg_space = DirectSum(UFLExprSpace(u), UFLExprSpace(g))
    tmp_u, tmp_g = ufl_arg_space.point()
    ufl_energy = (inner(tmp_g, tmp_g) + 1e-12) ** (pc / 2.0) / pc
    cr_ufl = CR_UFL_Expr(ufl_arg_space, ufl_energy, {tmp_u: 0, tmp_g: 1})

    cr_numpy = Parametric(cr_numpy_energy, 2, p)

    sigma = cr_ufl.target.point()
    form = sigma * dx(domain=mesh.ufl_domain())
    e_ufl = assemble_with_cr(form, cr_ufl, (u, grad(u)), sigma)
    e_numpy = assemble_with_cr(form, cr_numpy, (u, grad(u)), sigma)

    # Assert that the UFL-native and external calculations are the same
    assert_allclose(e_ufl, e_numpy)

    # Test that pyadjoint taped both functions identically
    rf_ufl = ReducedFunction(e_ufl, Control(pc))
    e_ufl2 = rf_ufl(3.0)
    rf_numpy = ReducedFunction(e_numpy, Control(p))
    e_numpy2 = rf_numpy(3.0)

    assert_allclose(e_ufl, e_numpy)

    # Run taylor tests with adjoint.
    h = create_overloaded_object(anp.array(1e-1))
    hc = Constant(h)
    v = AdjFloat(2.0)

    assert taylor_test(rf_ufl, Constant(3.0), v=v, h=hc) > 1.9
    assert taylor_test(rf_numpy, anp.array(3.0), v=v, h=h) > 1.9

    # Run taylor tests with tlm.
    assert taylor_test(rf_ufl, Constant(3.0), h=hc) > 1.9
    assert taylor_test(rf_numpy, anp.array(3.0), h=h) > 1.9


if __name__ == "__main__":
    test_vector()
    test_scalar()
