from crikit.cr.stdnumeric import type_tuple_to_space, RR, point_map
from crikit.cr.space_builders import DirectSum
from crikit.cr.map_builders import Parametric


def test_tuple_to_space():
    tup = (float, (float,) * 3)
    space = type_tuple_to_space(tup)
    assert space == DirectSum(RR, DirectSum(RR, RR, RR))


def test_point_map_decorator():
    @point_map(((float,) * 3, float), float, bare=True)
    def example(x, a):
        return (x[0] * x[0] + x[1] * x[1] + x[2] * x[2]) ** a

    @point_map(((float,) * 3, float), float)
    def example2(w):
        x = w[0]
        a = w[1]
        return (x[0] * x[0] + x[1] * x[1] + x[2] * x[2]) ** a

    x = (1.0, 2.0, 3.0)
    a = 4.0
    f = Parametric(example, (1,), (a,))
    f2 = Parametric(example, 1, a)
    f3 = Parametric(example, 1, a, bare=True)
    b = example((x, a))
    b2 = example2((x, a))
    b3 = f((x,))
    b4 = f((x,), (a,))
    b5 = f2((x,))
    b6 = f2((x,), a)
    b7 = f3(x)
    b8 = f3(x, a)

    assert b == b2
    assert b == b3
    assert b == b4
    assert b == b5
    assert b == b6
    assert b == b7
    assert b == b8
