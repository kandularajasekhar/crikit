import pytest

from crikit.cr.autograd import point_map
from crikit.covering import get_composite_cr, set_default_covering_params
from crikit.cr.numpy import CR_P_LaplacianNumpy
from crikit.cr.ufl import UFLFunctionSpace, UFLExprSpace
from crikit.cr.space_builders import DirectSum
from crikit.fe import *
from crikit.fe_adjoint import *
from crikit.group import SpecialOrthoGroup, GroupAction
from pyadjoint import *
from pyadjoint.enlisting import Enlist
from pyadjoint.overloaded_type import create_overloaded_object as coo
from pyadjoint_utils import *

from crikit.deprecated.invariants import TensorDefinition, get_invariant_maps
from numpy.testing import assert_allclose, assert_raises

import autograd.numpy as np


def test_vector():
    # Set up function spaces.
    mesh = UnitSquareMesh(3, 3)
    V = FunctionSpace(mesh, "P", 2)

    quad_params = {"quadrature_degree": 2}
    domain = mesh.ufl_domain()
    set_default_covering_params(domain=domain, quad_params=quad_params)

    # Create a test point map that is a function of an invariant quantity.
    p = AdjFloat(2.5)

    @point_map((-1,), (-1,), bare=True)
    def cr_invt(mag_squared):
        mu = (mag_squared + 1.0e-12) ** ((p - 2.0) / 2.0)
        return mu

    # Create a test input.
    if crikit_fe_backend == "firedrake":
        x = SpatialCoordinate(V.mesh())
        v = interpolate(x[0] * x[0] + x[1] * x[1], V)
    else:
        v = interpolate(Expression("x[0]*x[0] + x[1]*x[1]", degree=2), V)
    g = grad(v)

    # Create cr.
    source = UFLExprSpace(g)
    invariant_map = get_invariant_maps(2, (TensorDefinition(1, None),))
    cr = get_composite_cr(source, invariant_map, cr_invt)

    # Test rotations of the input.
    group = SpecialOrthoGroup(dim=g.ufl_shape[0])
    sigma = cr(g)

    group_map = GroupAction(group, invariant_map.source)
    cr_rot = get_composite_cr(source, group_map, invariant_map, cr_invt)
    for i in range(2):
        group_map.sample = group.sample()
        sigma_rot = cr_rot(g)
        assert_allclose(sigma, sigma_rot)


def test_group_applications():
    # Set up function spaces.
    mesh = UnitSquareMesh(3, 3)
    V = FunctionSpace(mesh, "P", 2)

    quad_params = {"quadrature_degree": 2}
    domain = mesh.ufl_domain()
    set_default_covering_params(domain=domain, quad_params=quad_params)

    # Create a test point map that is invariant under coordinate transformations.
    @point_map(((-1, 2), (-1, 2, 2), (-1, 2)), (-1,), bare=True)
    def cr_weighted_inner_product(x, M, y):
        My = np.matmul(M, y[:, :, None])
        xMy = np.matmul(x[:, None, :], My)
        return xMy.flatten()

    # Create a test input.
    if crikit_fe_backend == "firedrake":
        x = SpatialCoordinate(V.mesh())
        v = interpolate(x[0] * x[0] + x[1] * x[1], V)
        v2 = interpolate(-2 * x[0] * x[0] - 3 * x[1] * x[1], V)
    else:
        v = interpolate(Expression("x[0]*x[0] + x[1]*x[1]", degree=2), V)
        v2 = interpolate(Expression("-x[0]*x[0] - x[1]*x[1]", degree=2), V)
    x = grad(v)
    y = grad(v2)
    M = as_matrix([[dot(x, x), dot(x, y)], [dot(y, x), dot(y, y)]])

    # Test rotations of the input.
    group = SpecialOrthoGroup(dim=x.ufl_shape[0])

    test_input = (x, M, y)
    source = DirectSum([UFLExprSpace(e) for e in test_input])
    cr = get_composite_cr(source, cr_weighted_inner_product)
    w = cr(test_input)

    group_map = GroupAction(group, cr_weighted_inner_product.source)
    cr_rot = get_composite_cr(source, group_map, cr_weighted_inner_product)
    for i in range(2):
        group_map.sample = group.sample()
        w_rot = cr_rot(test_input)
        assert_allclose(w, w_rot)


def symmetrize(M):
    return (M + np.transpose(M, (0, 2, 1))) / 2


def asymmetrize(M):
    return (M - np.transpose(M, (0, 2, 1))) / 2


def random_sym(full_shape):
    return symmetrize(1 + np.random.rand(*full_shape))


def random_asym(full_shape):
    return asymmetrize(1 + np.random.rand(*full_shape))


def random_nosym(full_shape):
    return np.random.rand(*full_shape)


sym_makers = {"s": random_sym, "a": random_asym, None: random_nosym}


def invariant_setup(syms, n, dim, output_def):
    inputs = []
    t_defs = []
    for i, sym in enumerate(syms):
        rank = 1 if sym is None else 2
        full_shape = (n,) + rank * (dim,)
        inputs.append(coo(sym_makers[sym](full_shape)))
        t_defs.append(TensorDefinition(rank=rank, symmetry=sym))
    if len(inputs) == 1:
        inputs = inputs[0]
    inv_map = get_invariant_maps(dim=dim, input_defs=t_defs, output_def=output_def)
    if output_def.rank != 0:
        inv_map = inv_map[0]
    return inputs, t_defs, inv_map


all_syms = [
    ("s",),
    ("a",),
    ("s", "s"),
    ("a", "a"),
    ("s", "a"),
    ("s", "s", "s"),
    ("s", "s", "a"),
    ("s", "a", "a"),
    ("a", "a", "a"),
    (None,),
    ("s", None),
    ("a", None),
    ("s", "s", None),
    ("s", "a", None),
    ("a", "a", None),
    (None, None),
    ("s", None, None),
    ("a", None, None),
    ("s", "s", None, None),
    ("s", "a", None, None),
    ("a", "a", None, None),
    (None, None, None),
]

output_defs = [
    TensorDefinition(0, None),
    TensorDefinition(2, "s"),
]


@pytest.mark.parametrize("syms", all_syms)
@pytest.mark.parametrize("output_def", output_defs)
def test_group_invariance(syms, output_def):
    n = 5
    dim = 3

    group = SpecialOrthoGroup(dim=3)

    inputs, t_defs, inv_map = invariant_setup(syms, n, dim, output_def=output_def)
    inputs = Enlist(inputs)

    with push_tape():
        invariants = inv_map(inputs.delist())
        controls = tuple(Control(M) for M in inputs)
        rf = ReducedFunction(invariants, controls)

    with stop_annotating():
        # Check the number of invariants returned.
        expanded_invariants = (
            invariants
            if len(invariants.shape) == 2 + output_def.rank
            else invariants[:, None]
        )
        assert expanded_invariants.shape[1] == inv_map.num_invariants

        # The invariants should not change under group actions.
        for i in range(3):
            Q = group.sample()
            new_inputs = group.apply(Q, inputs)
            new_outputs = inv_map(inputs.delist(new_inputs))
            transformed_invariants = group.apply(
                Q, [invariants], offset=len(invariants.shape) - output_def.rank
            )[0]
            assert_allclose(transformed_invariants, new_outputs, atol=1e-10)

        # For random inputs, there shouldn't be an invariant that is all zeros.
        # A zero value likely indicates an incorrect invariant calculation.
        zero = np.zeros((n,) + (dim,) * output_def.rank)
        for j in range(expanded_invariants.shape[1]):
            with assert_raises(AssertionError):
                assert_allclose(expanded_invariants[:, j], zero, atol=1e-15)

        # Test adjoint and Hessian.
        h = tuple(1e-1 * np.random.randn(*a.shape) for a in inputs)
        v = tuple(np.random.randn(*a.shape) for a in Enlist(invariants))
        rate_keys = ["R0", "R1", "R2"]
        results = taylor_to_dict(rf, new_inputs, h, v=v)
        for order, k in enumerate(rate_keys):
            assert (
                min(results[k]["Rate"]) > order + 0.9
                or max(results[k]["Residual"]) < 1e-13
            ), results[k]

        # Test Jacobian action.
        rate_keys = ["R0", "R1"]
        results = taylor_to_dict(rf, new_inputs, h, Hm=0)
        for order, k in enumerate(rate_keys):
            assert min(results[k]["Rate"]) > order + 0.9, results[k]


def test_argument_order():
    n = 5
    dim = 3

    # Make sure that switching argument order gives the same result.
    with stop_annotating():
        inputs, t_defs, inv_map = invariant_setup(
            ("s", "a", None), n, dim, output_def=TensorDefinition(0, None)
        )

        invariants = inv_map(inputs)

        rev_inv_map = get_invariant_maps(dim=3, input_defs=t_defs[::-1])
        rev_invariants = rev_inv_map(inputs[::-1])

        # Switching the input order will also affect the output order, so I'm
        # going to sort the invariants to be able to compare the arrays.
        sorted_invariants = np.sort(invariants, axis=1)
        sorted_rev_invariants = np.sort(rev_invariants, axis=1)
        assert_allclose(sorted_invariants, sorted_rev_invariants, atol=1e-15)
