from crikit.cr.ufl import UFLFunctionSpace, CR_P_Laplacian
from crikit.fe import *
from crikit.fe_adjoint import *
from pyadjoint import AdjFloat, ReducedFunctional


def test_cr_p_laplacian():

    mesh = UnitSquareMesh(2, 2)
    V = FunctionSpace(mesh, "P", 1)
    if crikit_fe_backend == "firedrake":
        x = SpatialCoordinate(V.mesh())
        v = interpolate(x[0] * x[0] + x[1] * x[1], V)
    else:
        v = interpolate(Expression("x[0]*x[0] + x[1]*x[1]", degree=1), V)
    g = grad(v)
    p = Constant(0.5)
    cr = CR_P_Laplacian(p)

    sigma = cr((v, g))

    energy = assemble(inner(grad(v), sigma) * dx)

    r = ReducedFunctional(energy, Control(p))

    e2 = r(1.0)


def test_ufl_function_space():
    mesh = UnitSquareMesh(2, 2)

    V = FunctionSpace(mesh, "P", 1)
    space = UFLFunctionSpace(V)
    p = space.point()

    assert space.shape() == ()
    assert space.is_point(p)

    V_vec = VectorFunctionSpace(mesh, "P", 1)
    space_vec = UFLFunctionSpace(V_vec)
    p_vec = space_vec.point()

    assert space_vec.shape() == (2,)
    assert space_vec.is_point(p_vec)

    assert not space.is_point(p_vec)
    assert not space_vec.is_point(p)
