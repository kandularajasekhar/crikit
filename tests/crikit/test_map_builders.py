from crikit.cr.types import PointMap
from crikit.cr.space_builders import DirectSum
from crikit.cr.map_builders import AugmentPointMap
from crikit.cr.stdnumeric import RR


class PM(PointMap):
    def __init__(self):
        super().__init__(DirectSum(RR, RR), RR)

    def __call__(self, x, p=1, a=1):
        return x[0] ** p + x[1] ** p - a


def test_augment_point_map():
    pm = PM()
    x = (2, 1)
    p = 4
    a = 3

    aug = AugmentPointMap(pm, "a", RR)
    assert aug((x, a)) == pm(x, a=a)

    aug = AugmentPointMap(pm, "p", RR)
    assert aug((x, p)) == pm(x, p=p)

    aug = AugmentPointMap(pm, "p", RR, bare=True)
    assert aug((x, p)) == pm(x, p=p)

    aug = AugmentPointMap(
        pm,
        (
            "p",
            "a",
        ),
        DirectSum(RR, RR),
    )
    assert aug((x, (p, a))) == pm(x, p=p, a=a)

    aug = AugmentPointMap(
        pm,
        (
            "a",
            "p",
        ),
        DirectSum(RR, RR),
    )
    assert aug((x, (a, p))) == pm(x, p=p, a=a)

    aug = AugmentPointMap(
        pm,
        (
            "a",
            "p",
        ),
        DirectSum(RR, RR),
        bare=True,
    )
    assert aug((x, a, p)) == pm(x, p=p, a=a)
