import pytest

tf = pytest.importorskip("tensorflow.compat.v1")

from crikit.cr.ufl import UFLFunctionSpace
from crikit.cr.types import PointMap

try:
    from crikit.cr.cr_tensorflow import CRTensorFlow
except:
    pass
from crikit.cr.numpy import CR_P_LaplacianNumpy
from crikit.cr.stdnumeric import RR
from crikit.cr.map_builders import Parametric
from crikit.cr.space_builders import DirectSum
from crikit.cr.numpy import Ndarrays, CR_P_LaplacianNumpy
from crikit.fe import *
from crikit.fe_adjoint import *
from pyadjoint import AdjFloat
from pyadjoint.overloaded_type import create_overloaded_object
from numpy_adjoint import *
from pyadjoint_utils.numpy_adjoint import *
import numpy as np

try:
    tf.disable_eager_execution()
except:
    pass

from numpy.testing import assert_approx_equal, assert_allclose


def test_cr_numpy_ufl():
    mesh = UnitSquareMesh(5, 5)
    V = FunctionSpace(mesh, "P", 1)
    u = Function(V)

    n = len(u.vector())
    data = np.arange(n) + 1
    data = create_overloaded_object(data)

    p = AdjFloat(2)

    # Build graph.
    g = tf.Graph()
    with g.as_default():
        g_data = tf.placeholder(
            dtype=tf.float64, shape=(None, *data.shape[1:]), name="data"
        )
        g_p = tf.constant(p, dtype=tf.float64, name="p")

        g_output = g_data ** g_p
    sess = tf.Session(graph=g)

    # Create numpy CR.
    cr = CRTensorFlow((g_data, g_p), g_output, sess)
    output_space = Ndarrays((-1,) + data.shape[1:])
    input_space = DirectSum(output_space, Ndarrays(()))
    assert cr.source == input_space
    assert cr.target == output_space

    # Call cr.
    correct_output = data ** p
    output = cr((data, p))
    assert output_space.is_point(output)
    assert_allclose(output, correct_output)

    # Call cr with different values.
    data_rev = np.arange(n)[::-1]
    data_rev = create_overloaded_object(data_rev)
    correct_output = data_rev ** 3
    assert_allclose(cr((data_rev, AdjFloat(3))), correct_output)

    # Create UFL CR.
    def outputs_conversion(outputs):
        f = Function(V)
        f.vector()[:] = outputs[0]
        return [f]

    def adj_outputs_conversion(adj_outputs):
        adj_u = Function(V)
        adj_u.vector()[:] = adj_outputs[g_data]
        adj_outputs[g_data] = adj_u.vector()
        return adj_outputs

    output_space = UFLFunctionSpace(V)
    input_space = DirectSum(output_space, RR)
    tf_kwargs = {
        "outputs_conversion": outputs_conversion,
        "adj_outputs_conversion": adj_outputs_conversion,
    }
    cr = CRTensorFlow(
        (g_data, g_p),
        g_output,
        sess,
        tf_kwargs=tf_kwargs,
        input_space=input_space,
        output_space=output_space,
    )
    assert input_space == cr.source
    assert output_space == cr.target

    # Call cr.
    u.vector()[:] = data
    correct_output = data ** p
    output = cr((u, p))
    assert output_space.is_point(output)
    assert_allclose(output.vector()[:], correct_output)

    # Call cr with different values.
    data_rev = np.arange(n)[::-1]
    u_rev = Function(V)
    u_rev.vector()[:] = data_rev
    correct_output = data_rev ** 3
    assert_allclose(cr((u_rev, AdjFloat(3))).vector()[:], correct_output)

    # Make sure it works with Parametric.
    f = Parametric(cr, 1, AdjFloat(2), bare=True)
    assert_allclose(f(u).vector()[:], data ** 2)
    assert_allclose(f(u, AdjFloat(3)).vector()[:], data ** 3)

    f = Parametric(cr, 1, AdjFloat(2), bare=False)
    assert_allclose(f((u,)).vector()[:], data ** 2)
    assert_allclose(f((u,), AdjFloat(3)).vector()[:], data ** 3)

    sess.close()


def test_cr_vector_numpy():
    n = 21
    shape = (-1, 2)
    data = np.arange(2 * n).reshape(shape)
    data = create_overloaded_object(data)

    p = AdjFloat(4)

    # Build graph.
    g = tf.Graph()
    with g.as_default():
        g_data = tf.placeholder(dtype=tf.float64, shape=(None, 2), name="data")
        g_p = tf.constant(p, dtype=tf.float64, name="p")

        g_scale = g_data * g_data
        with tf.name_scope("reduce_sum"):
            g_scale = tf.reduce_sum(g_scale, axis=1) + 1e-12

        g_output = tf.expand_dims(g_scale ** ((g_p - 2) / 2), 1)
        g_output = g_output * g_data

    sess = tf.Session(graph=g)

    cr_tf = CRTensorFlow((g_data, g_p), g_output, sess)
    cr_hand = CR_P_LaplacianNumpy(input_u=False)

    # Call cr.
    cr_hand.setParams(p)
    hand_output = cr_hand(data)
    tf_output = cr_tf((data, p))
    assert_allclose(hand_output, tf_output)

    # Call cr with different values.
    data_rev = data[::-1]
    data_rev = create_overloaded_object(data_rev)
    cr_hand.setParams(AdjFloat(3))
    hand_output = cr_hand(data_rev)
    tf_output = cr_tf((data_rev, AdjFloat(3)))
    assert_allclose(hand_output, tf_output)
