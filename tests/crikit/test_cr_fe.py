from crikit.cr.quadrature import estimate_total_polynomial_degree as est_degree
from crikit.fe import *
from ufl import TensorElement, Coefficient
from ufl import FunctionSpace as uflFunctionSpace


def test_est_degree():
    p = 2

    mesh = UnitSquareMesh(2, 2)
    V = FunctionSpace(mesh, "P", p)
    u = Function(V)

    sigma = Coefficient(uflFunctionSpace(None, TensorElement("Real", shape=())))

    form = u * sigma * dx
    assert est_degree(form, default_degree=2) == (p + 2)

    sigma_map = {sigma: 4}
    assert est_degree(form, default_degree=1, coefficient_replace_map=sigma_map) == (
        p + 4
    )

    sigma_map = {sigma.ufl_element(): V.ufl_element()}
    assert est_degree(form, default_degree=1, element_replace_map=sigma_map) == (p + p)
