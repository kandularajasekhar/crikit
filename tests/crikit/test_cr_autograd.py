import autograd.numpy as np
from autograd import vector_jacobian_product

from crikit.cr.space_builders import DirectSum
from crikit.cr.numpy import Ndarrays
from crikit.cr.stdnumeric import RR
from crikit.cr.autograd import AutogradPointMap
from pyadjoint_utils.numpy_adjoint import ndarray
from pyadjoint.overloaded_type import create_overloaded_object as coo
from pyadjoint import get_working_tape, stop_annotating
from pyadjoint.enlisting import Enlist
from pyadjoint_utils import (
    AdjFloat,
    Control,
    ReducedFunction,
    ReducedFunctionNumPy,
    taylor_to_dict,
)


def test_cr_autograd():
    def cr(u, g, p):
        ggt = np.matmul(g[:, :, None], g[:, None, :])
        tr = np.trace(ggt, axis1=1, axis2=2)
        mu = (np.sum(g * g, axis=1) + 1.0e-12) ** ((p - 2.0) / 2.0) + tr
        return np.reshape(mu, mu.shape + (1,)) * g

    source = DirectSum(Ndarrays((-1,)), Ndarrays((-1, 3)), RR)
    target = Ndarrays((-1, 3))

    cr_ag = AutogradPointMap(
        source, target, cr, bare=True, pointwise=(True, True, False)
    )

    n = 10
    u = coo(np.arange(n, dtype=float))
    g = coo(np.reshape(np.arange(3 * n, dtype=float) + 3, (n, 3)))
    p = coo(np.array(2.5))
    cr_point = (u, g, p)

    with stop_annotating():
        outraw = cr(*cr_point)
    rf_point = (u, g, p)
    controls = tuple(Control(a) for a in rf_point)
    outputs = cr_ag(cr_point)
    assert (outraw == outputs).all()

    J = ReducedFunction(outputs, controls)

    outrecomp = J(rf_point)
    assert (outrecomp == outputs).all()

    h = tuple(coo(np.array(np.random.randn(*a.shape))) for a in rf_point)
    v = [coo(np.random.randn(*outputs.shape))]

    rate_keys = ["R0", "R1", "R2"]
    results = taylor_to_dict(J, rf_point, h, v=v)
    for order, k in enumerate(rate_keys):
        assert min(results[k]["Rate"]) > order + 0.9, results[k]

    # Test tlm matrix.
    tlm_matrix = J.jac_matrix()

    outputs = Enlist(outputs)
    tlm_matrix = [tlm_matrix]

    dJdm = [0] * len(outputs)
    v = [coo(v_i) for v_i in v]
    for i in range(len(outputs)):
        for j in range(len(controls)):
            if cr_ag.pointwise[j]:
                rank = len(h[j].shape) - 1
                hdot_shape = tlm_matrix[i][j].shape[
                    : len(tlm_matrix[i][j].shape) - rank
                ]
                w = (
                    np.tensordot(a, b, axes=rank)
                    for a, b in zip(tlm_matrix[i][j], h[j])
                )
                hdot = np.zeros(hdot_shape)
                for k, hdot_k in enumerate(w):
                    hdot[k, ...] = hdot_k
            else:
                hdot = np.tensordot(tlm_matrix[i][j], h[j], axes=len(h[j].shape))
            dJdm[i] += v[i]._ad_dot(hdot)
    dJdm = [coo(dJdm_i) for dJdm_i in dJdm]

    rate_keys = ["R0", "R1"]
    results = taylor_to_dict(J, rf_point, h, dJdm=dJdm[0], Hm=0, v=v)
    for order, k in enumerate(rate_keys):
        assert min(results[k]["Rate"]) > order + 0.9, results[k]


if __name__ == "__main__":
    test_cr_autograd()
