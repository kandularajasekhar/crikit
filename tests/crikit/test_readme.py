from crikit import *
import jax
from jax import numpy as jnp
import numpy as np
import matplotlib.pyplot as plt
from functools import partial


def test_readme():
    # Optimization options for the form compiler
    parameters["form_compiler"]["cpp_optimize"] = True
    ffc_options = {
        "optimize": True,
        "eliminate_zeros": True,
        "precompute_basis_const": True,
        "precompute_ip_const": True,
    }

    # based on https://comet-fenics.readthedocs.io/en/latest/demo/elasticity/2D_elasticity.py.html
    fe_order = 2
    dims = 2
    Nx, Ny = 20, 2  # 50, 5
    L = 20.0
    H = 1.0
    mesh = RectangleMesh(Point(0.0, 0.0), Point(L, H), Nx, Ny)
    V = VectorFunctionSpace(mesh, "CG", fe_order)
    quad_params = {"quadrature_degree": fe_order + 1}
    set_default_covering_params(domain=mesh.ufl_domain(), quad_params=quad_params)
    u = Function(V)

    def left_boundary(x, on_boundary):
        return near(x[0], 0.0)

    bcs = [DirichletBC(V, Constant((0.0, 0.0)), left_boundary)]

    def apply_bcs(bcs, u):
        if hasattr(u, "vector"):
            for bc in bcs:
                bc.apply(u.vector())
        else:
            for bc in bcs:
                bc.apply(u)

        return u

    u = Function(V)
    v = TestFunction(V)
    # external forces
    f = Constant((0.0, -1e-3), name="force")
    eps = sym(grad(u))

    input_types = (TensorType.make_symmetric(2, dims, "strain"),)
    output_type = TensorType.make_symmetric(2, dims, "stress")

    def cr_func(invts, theta):
        lmbda, mu = theta
        return jnp.array([lmbda * jnp.log1p(invts[0]), 2 * mu])

    E_true = 1.0e5
    nu_true = 0.3
    lmbda_true = (E_true * nu_true) / ((1 + nu_true) * (1 - 2 * nu_true))
    mu_true = E_true / (2 * (1 + nu_true))
    lmbda_true = 2 * mu_true * lmbda_true / (lmbda_true + 2 * mu_true)
    theta = array([lmbda_true, mu_true])
    cr_true = CR(output_type, input_types, cr_func, params=[theta])

    target_shape = tuple(i for i in cr_true.target.shape() if i != -1)

    (standin_sigma,) = create_ufl_standins((target_shape,))

    form = inner(standin_sigma, sym(grad(v))) * dx - inner(f, v) * dx
    # define a new sub-tape that records the actions of this equation
    with push_tape():
        # a function that we can assemble the variational form into
        # using the `tensor` kwarg of `crikit.assemble()`, which
        # is directly passed on to `crikit.fe.backend.assemble()`
        # (e.g. `fenics.assemble()`)
        residual = Function(V)

        # input to the CR is sym(grad(u))

        assemble_with_cr(
            form,
            cr_true,
            sym(grad(u)),
            standin_sigma,
            tensor=residual,
            quad_params=quad_params,
        )
        ucontrol = Control(u)
        # a ReducedFunction to represent the residual as a function of `u`
        res_rf = ReducedFunction(residual, ucontrol)

    red_eq = ReducedEquation(
        res_rf, partial(apply_bcs, bcs), partial(apply_bcs, homogenize_bcs(bcs))
    )

    solver = SNESSolver(red_eq, {"jmat_type": "assembled"})
    pred_u = solver.solve(ucontrol, ksp_type="preonly", pc_type="lu")
    print("theta is ", theta)
    rf_val = integral_loss(
        pred_u, interpolate(Expression(("x[0]", "x[1]"), degree=1), V)
    )
    rf = ReducedFunctional(rf_val, Control(theta))
    assert (
        taylor_test(
            rf, theta, array(1e4 * np.random.randn(*theta.shape)), v=AdjFloat(1.0)
        )
        >= 1.9
    )
