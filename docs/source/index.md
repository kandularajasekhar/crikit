# Welcome to CRIKit!

Documentation is here: {ref}`documentation <crikit-documentation>`.

```{toctree}
:titlesonly: true
:maxdepth: 2
:hidden: true

About <about/index.rst>
Documentation <documentation/index.rst>

```

## Indices and tables

* {ref}`genindex`
* {ref}`modindex`
* {ref}`search`


