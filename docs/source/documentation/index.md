(crikit-documentation)=

# Welcome to CRIKit's documentation!

Tutorial can be found here: {ref}`tutorial <crikit-tutorial>`.

Core documentation is here: {ref}`CRIKit core <crikit-core-docs>`.

API Reference can be found here: {ref}`crikit API reference <crikit-api-reference>`.

Documentation on work on extending Pyadjoint is found here: {ref}`API <pyadjoint-api-reference>` and {ref}`description <pyadjoint-extension-docs>`.

## Table of contents

```{toctree}
:maxdepth: 1

crikit_core.md
ad_systems.md
api.md
tutorial.md
pyadjoint/core.md
pyadjoint/api.md

```

## Indices and tables

* {ref}`genindex`
* {ref}`modindex`
* {ref}`search`


