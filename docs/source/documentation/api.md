(crikit-api-reference)=

# crikit API reference

:::{contents} Table of Contents
:local: true
:backlinks: none

:::

## Core classes

```{eval-rst}
.. automodule:: crikit.cr.types
```

```{eval-rst}
.. autoclass:: Space
   :members:
```

```{eval-rst}
.. autoclass:: PointMap
   :members:

```

## Space builders

```{eval-rst}
.. automodule:: crikit.cr.space_builders
```

```{eval-rst}
.. autoclass:: DirectSum
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: Multiset
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autofunction:: enlist
```

## Point map builders

```{eval-rst}
.. automodule:: crikit.cr.map_builders
```

```{eval-rst}
.. autoclass:: Callable
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: Parametric
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: AugmentPointMap
   :show-inheritance:
   :members:

```

```{eval-rst}
.. autoclass:: CompositePointMap
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: ParallelPointMap
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: IdentityPointMap
   :show-inheritance:
   :members:

```

## CR Implementations

### crikit.cr.cr

```{eval-rst}
.. automodule:: crikit.cr.cr
```

```{eval-rst}
.. autoclass:: CR
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autofunction:: cr_function_shape
```

```{eval-rst}
.. autofunction:: save_jax_cr
```

```{eval-rst}
.. autoclass:: RivlinModel
   :show-inheritance:
   :members:
```

```{eval-rst}
.. automodule:: crikit.cr.jax_utils

```

```{eval-rst}
.. autoclass:: JAXArrays
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: JAX_To_UFLFunctionSpace
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: UFLExprSpace_To_JAX
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: JAX_UFLFunctionSpace_Covering
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: ReducedFunctionJAX
   :show-inheritance:
   :members:

```

### crikit.cr.numpy

```{eval-rst}
.. automodule:: crikit.cr.numpy
```

```{eval-rst}
.. autoclass:: Ndarrays
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: CR_P_LaplacianNumpy
   :show-inheritance:
   :members:
```

### crikit.cr.autograd

```{eval-rst}
.. automodule:: crikit.cr.autograd
```

```{eval-rst}
.. autoclass:: AutogradPointMap
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autofunction:: point_map
```

### crikit.cr.ufl

```{eval-rst}
.. automodule:: crikit.cr.ufl
```

```{eval-rst}
.. autoclass:: UFLFunctionSpace
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: UFLExprSpace
   :show-inheritance:
   :members:

```

```{eval-rst}
.. autoclass:: CR_UFL_Expr
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: CR_P_Laplacian
   :show-inheritance:
   :members:

```

### crikit.cr.stdnumeric

```{eval-rst}
.. automodule:: crikit.cr.stdnumeric
```

```{eval-rst}
.. autodata:: ZZ
```

```{eval-rst}
.. autodata:: RR
```

```{eval-rst}
.. autodata:: CC
```

```{eval-rst}
.. autofunction:: type_tuple_to_space
```

```{eval-rst}
.. autofunction:: point_map

```

## Covering

```{eval-rst}
.. automodule:: crikit.covering.covering
```

```{eval-rst}
.. autoclass:: Covering
   :members:
```

```{eval-rst}
.. autofunction:: get_default_covering_params
```

```{eval-rst}
.. autofunction:: set_default_covering_params
```

```{eval-rst}
.. autofunction:: reset_default_covering_params
```

```{eval-rst}
.. autofunction:: register_covering
```

```{eval-rst}
.. autofunction:: get_map
```

```{eval-rst}
.. autofunction:: get_composite_cr

```

## Covering Implementations

### crikit.covering.ufl

```{eval-rst}
.. automodule:: crikit.covering.ufl
```

```{eval-rst}
.. autoclass:: Numpy_UFLFunctionSpace_Covering
   :show-inheritance:
```

```{eval-rst}
.. autoclass:: UFLFunctionSpace_UFLExpr_Covering
   :show-inheritance:
```

```{eval-rst}
.. autoclass:: UFLFunctionSpace_UFLFunctionSpace_Covering
   :show-inheritance:
```

```{eval-rst}
.. autoclass:: Numpy_To_UFLFunctionSpace
   :show-inheritance:
```

```{eval-rst}
.. autoclass:: UFLExprSpace_To_Numpy
   :show-inheritance:
```

<!---
autoclass::To_UFLFunctionSpace
:show-inheritance:
-->

## Invariants

```{eval-rst}
.. automodule:: crikit.invariants
```

```{eval-rst}
.. autoclass:: TensorType
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autoclass:: LeviCivitaType
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autofunction:: levi_civita
```

```{eval-rst}
.. autofunction:: type_from_array

```

```{eval-rst}
.. autoclass:: InvariantInfo
   :show-inheritance:
   :members:
```

```{eval-rst}
.. autofunction:: get_invariant_functions
```

```{eval-rst}
.. autofunction:: get_invariant_descriptions
```

```{eval-rst}
.. autofunction:: register_invariant_functions

```

```{eval-rst}
.. autofunction:: near
```

```{eval-rst}
.. autofunction:: symm
```

```{eval-rst}
.. autofunction:: antisymm
```

## FE Support

```{eval-rst}
.. automodule:: crikit.cr.fe
```

```{eval-rst}
.. autofunction:: assemble_with_cr

```

## Observers

```{eval-rst}
.. automodule:: crikit.observer
```

```{eval-rst}
.. autoclass:: AdditiveRandomFunction
   :members:
```

```{eval-rst}
.. autoclass:: SubdomainObserver
   :members:
```

```{eval-rst}
.. autoclass:: SurfaceObserver
   :members:
```

## Groups

```{eval-rst}
.. automodule:: crikit.group
```

```{eval-rst}
.. autoclass:: SpecialOrthoGroup
   :members:
```

```{eval-rst}
.. autofunction:: get_einsum_args
```

```{eval-rst}
.. autoclass:: GroupAction
   :show-inheritance:
   :members:


```

## Logging

```{eval-rst}
.. automodule:: crikit.logging
```

```{eval-rst}
.. autofunction:: set_log_level
```


