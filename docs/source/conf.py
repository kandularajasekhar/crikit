# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

import os

# -- Project information -----------------------------------------------------

project = "CRIKit"
copyright = "2020, CRIKit Team"
author = "CRIKit Team"

# The program will crash if FEniCS is imported before tensorflow.
try:
    import tensorflow
except ImportError:
    pass

# Read version from file.
version_file = os.path.join(os.path.dirname(__file__), "../../crikit/_version.py")
with open(version_file, "r") as f:
    version = f.read().split("=", 1)[1].strip(" \n\"'")

# The full version, including alpha/beta/rc tags.
release = version

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "myst_nb",
    "sphinx.ext.autodoc",
    "sphinx.ext.coverage",
    "sphinx.ext.doctest",
    "sphinx.ext.intersphinx",
    "sphinx.ext.mathjax",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinxcontrib.bibtex",
    "sphinxcontrib.katex",
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Warn about broken links.
nitpicky = True

# -- Extension-specific configuration ----------------------------------------

# Include constructor by default in autodoc.
autodoc_default_options = {
    "special-members": "__init__",
}

# Include other functions that start or end with a double underscore.
napoleon_include_special_with_doc = True

# Don't repeat the parent's documentation in each child class.
autodoc_inherit_docstrings = False

# We can reference standard python things like int and it will link to its documentation.
intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
    "numpy": ("https://docs.scipy.org/doc/numpy/", None),
    "ufl": ("https://fenics.readthedocs.io/projects/ufl/en/latest/", None),
    "pyadjoint": ("http://www.dolfin-adjoint.org/en/latest/", None),
    "jax": ("https://jax.readthedocs.io/en/latest/", None),
}

# This lets us put "optional" in the type list of a function argument.
nitpick_ignore = [("py:class", "optional"), ("py:class", "function")]

# Set some useful flags for doctest.
import doctest

doctest_default_flags = (
    doctest.NORMALIZE_WHITESPACE
    | doctest.IGNORE_EXCEPTION_DETAIL
    | doctest.ELLIPSIS
    | doctest.DONT_ACCEPT_TRUE_FOR_1
)

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "pydata_sphinx_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']

bibtex_bibfiles = ["references.bib"]
bibtex_default_style = "unsrt"
bibtex_reference_style = "author_year"

myst_enable_extensions = [
    "deflist",
    "dollarmath",
    "html_image",
    "linkify",
    "colon_fence",
]

myst_heading_anchors = 2
myst_url_schemes = ("http", "https", "mailto")

nb_render_priority = {
    "html": (
        "application/vnd.jupyter.widget-view+json",
        "application/javascript",
        "text/html",
        "image/svg+xml",
        "image/png",
        "image/jpeg",
        "text/markdown",
        "text/latex",
        "text/plain",
    ),
    "doctest": (
        "application/vnd.jupyter.widget-view+json",
        "application/javascript",
        "text/html",
        "image/svg+xml",
        "image/png",
        "image/jpeg",
        "text/markdown",
        "text/latex",
        "text/plain",
    ),
}
