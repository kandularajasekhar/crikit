from ..reduced_function import ReducedFunction
from pyadjoint.enlisting import Enlist


class ReducedEquation(object):
    """A class that encapsulates all the information required to formulate a
    reduced equation solve problem."""

    """
        reduced_function: a ReducedFunction that calculates the residual as a function of x.
        bcs: a function or list of functions that apply boundary conditions.
        h_bcs: a function or list of functions that apply homogenenous boundary conditions.
    """

    def __init__(self, reduced_function, bcs=None, h_bcs=None):
        bcs = [] if bcs is None else Enlist(bcs)
        h_bcs = [] if h_bcs is None else Enlist(h_bcs)

        self.__check_arguments(reduced_function, bcs, h_bcs)

        self.reduced_function = reduced_function
        self.bcs = bcs
        self.h_bcs = h_bcs

    def __check_arguments(self, reduced_function, bcs, h_bcs):

        if not isinstance(reduced_function, ReducedFunction):
            raise TypeError("reduced_function should be a ReducedFunction")

        for bc in bcs:
            if not callable(bc):
                raise TypeError("Boundary conditions must be callable")

        for h_bc in h_bcs:
            if not callable(h_bc):
                raise TypeError("Homogeneous boundary conditions must be callable")
