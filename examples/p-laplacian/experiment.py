from fenics import *
from fenics_adjoint import *
from pyadjoint_utils import *
from pyadjoint_utils.fenics_adjoint import homogenize_bcs
from crikit.projection import project as myproject
from crikit.cr.fe import assemble_with_cr
from crikit.cr.ufl import create_ufl_standins


class Experiment:
    def __init__(self, alpha=0, g=9.8, rho=1, input_u=False, energy_cr=False):
        self.setF(alpha=alpha, g=g, rho=rho)
        self.input_u = input_u
        self.energy_cr = energy_cr

    def setF(self, alpha=None, g=None, rho=None):
        self.alpha = self.alpha if alpha is None else alpha
        self.g = self.g if g is None else g
        self.rho = self.rho if rho is None else rho
        self.f = Constant(-self.rho * self.g * cos(self.alpha), name="f")

    def setDomainParameters(self, V, bcs):
        self.V = V  # function space
        self.bcs = bcs  # boundary conditions
        if not isinstance(self.bcs, (list, tuple)):
            self.bcs = [self.bcs]
        self.h_bcs = homogenize_bcs(self.bcs)

    def _ufl_solve(self, cr, u, inputs, solver_parameters=None):
        if solver_parameters is None:
            solver_parameters = {}

        # Set up the weak form.
        sigma = cr(inputs)
        F = self.get_form(sigma, u)

        # Compute solution.
        solve(F == 0, u, self.bcs, solver_parameters=solver_parameters)
        return u

    def bc(self, u):
        for bc in self.bcs:
            bc.apply(u.vector())
        return u

    def h_bc(self, u):
        for bc in self.h_bcs:
            bc.apply(u.vector())
        return u

    def run(self, cr, observer=None, initial_u=None, ufl=False):
        # Define u.
        if initial_u is None:
            u = Function(self.V, name="w")
        else:
            u = initial_u

        if self.input_u:
            inputs = [u, grad(u)]
        else:
            inputs = grad(u)

        if ufl:
            u = self._ufl_solve(cr, u, inputs)
        else:
            u = self._solve(cr, u, inputs)
        if observer is None:
            return u
        return observer(u)

    def _solve(self, cr, u, inputs, **kwargs):
        if self.energy_cr:
            E, sigma = self.get_cr_energy(cr, u)
            with push_tape():

                def derivative_cb_post(energy, dJdu, u):
                    return self.h_bc(dJdu)

                energy = assemble_with_cr(E, cr, inputs, sigma)

                ucontrol = Control(u)
                energy_rf = ReducedFunctional(
                    energy,
                    ucontrol,
                    eval_cb_pre=self.bc,
                    derivative_cb_post=derivative_cb_post,
                )

            # Solve for u by minimizing energy.
            # TODO: this solve needs to be annotated.
            u = minimize(energy_rf, options={"disp": True})
        else:
            F, sigma = self.get_cr_form(cr, u)
            with push_tape():
                residual = Function(self.V)
                assemble_with_cr(F, cr, inputs, sigma, tensor=residual)

                ucontrol = Control(u)
                residual_rf = ReducedFunction(residual, ucontrol)

            reduced_equation = ReducedEquation(residual_rf, self.bc, self.h_bc)

            solver = SNESSolver(reduced_equation, {"jmat_type": "action"})
            u = solver.solve(ucontrol, disp=True)

        return u

    def get_cr_form(self, cr, u):
        target_shape = tuple(i for i in cr.target.shape() if i != -1)
        sigma = create_ufl_standins((target_shape,))[0]
        return self.get_form(sigma, u), sigma

    def get_cr_energy(self, cr, u):
        sigma = create_ufl_standins((cr.target.shape()[1:],))[0]
        return self.get_energy(sigma, u), sigma

    def get_form(self, sigma, u):
        v = TestFunction(self.V)
        if len(sigma.ufl_shape) == 0:
            sigma = sigma * grad(u)
        F = (dot(sigma, grad(v)) - self.f * v) * dx
        return F

    def get_energy(self, sigma, u):
        E = (sigma - self.f * u) * dx
        return E

    def get_residual(self, cr, u):
        if self.input_u:
            inputs = [u, grad(u)]
        else:
            inputs = grad(u)

        if self.energy_cr:
            E, sigma = self.get_cr_energy(cr, u)
            continue_annotation()
            with push_tape():
                energy = assemble_with_cr(E, cr, inputs, sigma)
                ucontrol = Control(u)
                energy_rf = ReducedFunctional(energy, ucontrol)
                res = energy_rf.derivative()
            pause_annotation()
        else:
            F, sigma = self.get_cr_form(cr, u)
            res = Function(self.V)
            assemble_with_cr(F, cr, inputs, sigma, tensor=res)

        for bc in self.h_bcs:
            bc.apply(res.vector())
        return res
