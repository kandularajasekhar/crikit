#!/usr/bin/python3
from __future__ import print_function

try:
    import tensorflow as tf
    from tensorflow.keras import layers

    tensorflow_exception = None
except ImportError as e:
    tensorflow_exception = e
from crikit.fe import *
from crikit.fe_adjoint import *
from numpy_adjoint import *
from pyadjoint.overloaded_type import create_overloaded_object
from pyadjoint.enlisting import Enlist
from pyadjoint.reduced_functional_numpy import ReducedFunctionalNumPy
from pyadjoint import *
from pyadjoint_utils import *
from pyadjoint_utils.fenics_adjoint import *
from pyadjoint_utils.tensorflow_adjoint import *
from pyadjoint_utils.numpy_adjoint import *
from crikit.cr.autograd import point_map as autograd_point_map
from crikit.cr.numpy import Ndarrays
from crikit.cr.ufl import UFLFunctionSpace, UFLExprSpace, CR_UFL_Expr, CR_P_Laplacian
from crikit.cr.quadrature import make_quadrature_spaces
from crikit.cr.space_builders import DirectSum
from crikit.cr.cr_tensorflow import CRTensorFlow
from crikit.cr.cr_network import CRNetwork
from crikit.cr.map_builders import CompositePointMap, Parametric
from crikit.overloaded_network import Network, PlapNetwork
from crikit.network_block import get_num_network_inputs_outputs
from crikit.loss import integral_loss, vector_loss
from crikit.observer import u_observer, SurfaceObserver
from crikit.covering import set_default_covering_params, get_composite_cr
from crikit.projection import project
import argparse
import autograd.numpy as anp
import numpy as np
import crikit.utils as utils

try:
    import matplotlib.pyplot as plt
except:
    pass

from network_builders import build_input_convex_network, build_deep_network
from experiment import Experiment

# from crikit.invariant import VariantCR, IsotropicVectorMapper, DummyMapper, IOInfo

EX_CMAP = None
GRADU_CMAP = None
XY_CMAP = None


def create_tensorflow_network():
    use_u_input = args.input_u
    scalar_cr = not args.vector_cr

    num_inputs, num_outputs = get_num_network_inputs_outputs(
        output_space, *input_spaces
    )
    u_size = 1 * use_u_input
    gradu_size = num_inputs - u_size

    tf.keras.backend.set_floatx("float64")
    g = tf.Graph()
    with g.as_default():
        tf.set_random_seed(args.seed)

        g_gradu = tf.placeholder(
            dtype=tf.float64, shape=(None, gradu_size), name="gradu_data"
        )
        if use_u_input:
            g_u = tf.placeholder(dtype=tf.float64, shape=(None,), name="u_data")
            g_u_shaped = tf.reshape(g_u, (-1, 1))
            g_inputs = layers.concatenate([g_u_shaped, g_gradu])
        else:
            g_inputs = g_gradu

        # layer_sizes = [5, 5, 5, 5]
        layer_sizes = [5]
        if scalar_cr:
            g_output = build_input_convex_network(g_inputs, layer_sizes, num_outputs)
        else:
            g_output = build_deep_network(g_inputs, layer_sizes, num_outputs)

        init = tf.global_variables_initializer()

        sess = tf.Session()
        sess.run(init)

    if use_u_input:
        g_inputs = [g_u, g_gradu]
    else:
        g_inputs = g_gradu

    return sess, g_inputs, g_output


def create_tensorflow_plap_network():
    use_u_input = args.input_u

    num_inputs, num_outputs = get_num_network_inputs_outputs(
        output_space, *input_spaces
    )
    u_size = 1 * use_u_input
    gradu_size = num_inputs - u_size

    g = tf.Graph()
    with g.as_default():
        g_input = tf.placeholder(
            dtype=tf.float64, shape=(None, gradu_size), name="data"
        )
        g_p = tf.get_variable(
            dtype=tf.float64, shape=(), initializer=tf.zeros_initializer(), name="p"
        )

        g_scale = g_input * g_input
        with tf.name_scope("reduce_sum"):
            g_scale = tf.reduce_sum(g_scale, axis=1) + 1e-12

        if args.energy_cr:
            g_output = g_scale ** (g_p / 2) / g_p
        else:
            g_output = g_scale ** ((g_p - 2) / 2)

        if args.vector_cr:
            g_output = tf.expand_dims(g_output, 1) * g_input

        init = tf.global_variables_initializer()

        sess = tf.Session()
        sess.run(init)

    return sess, g_input, g_output


def run_network_tensorflow_jacobian(
    args, ex, obs, obs_u, observer, loss, obsCR, record_stem="", opt_output=None
):
    if tensorflow_exception is not None:
        raise tensorflow_exception

    sess, g_inputs, g_output = create_tensorflow_network()

    cr = CRTensorFlow(g_inputs, g_output, sess)
    run_network_cr(
        args, ex, obs, obs_u, observer, loss, obsCR, cr, record_stem, opt_output
    )

    sess.close()


def run_network_tensorflow_invariant(
    args, ex, obs, obs_u, observer, loss, obsCR, record_stem="", opt_output=None
):
    if tensorflow_exception is not None:
        raise tensorflow_exception

    @autograd_point_map((-1, 2), (-1, 1), bare=True)
    def get_invariant_input(g):
        return anp.sum(g * g, axis=1)[:, None] + 1e-12
        # return anp.sqrt(anp.sum(g*g, axis=1)[:, None] + 1e-12)

    sess, g_inputs, g_output = create_tensorflow_network()
    inner_cr = CRTensorFlow(g_inputs, g_output, sess)

    cr = CompositePointMap(get_invariant_input, inner_cr)

    def setParams(params):
        inner_cr.setParams(params)
        cr.params = inner_cr.params
        cr.params_controls = inner_cr.params_controls

    cr.setParams = setParams
    cr.setParams(inner_cr.params)

    run_network_cr(
        args, ex, obs, obs_u, observer, loss, obsCR, cr, record_stem, opt_output
    )

    sess.close()


def run_plap_tensorflow_jacobian(
    args, ex, obs, obs_u, observer, loss, obsCR, record_stem="", opt_output=None
):
    if tensorflow_exception is not None:
        raise tensorflow_exception

    sess, g_inputs, g_output = create_tensorflow_plap_network()
    cr = CRTensorFlow(g_inputs, g_output, sess)

    p = create_overloaded_object(np.array(args.startp))
    cr.setParams([p])
    run_network_cr(
        args, ex, obs, obs_u, observer, loss, obsCR, cr, record_stem, opt_output
    )

    sess.close()


def run_network(
    args,
    ex,
    obs,
    obs_u,
    observer,
    loss,
    obsCR,
    network,
    record_stem="",
    opt_output=None,
):
    cr = make_cr_network(input_spaces, output_space, network)
    run_network_cr(
        args, ex, obs, obs_u, observer, loss, obsCR, cr, record_stem, opt_output
    )


def run_plap_invariant(
    args, ex, obs, obs_u, observer, loss, obsCR, record_stem="", opt_output=None
):
    V_vec = VectorFunctionSpace(mesh, "CG", 1)

    isotropic = IsotropicVectorMapper(V_vec)

    p = Constant(2.5, name="p")

    def plapInvariantCR(magSquared):
        return sqrt(magSquared) * (magSquared + 1e-12) ** ((p - 2) / 2)

    # The variant CR will take grad(u) as input.
    input_desc = (IOInfo(V_vec, isotropic),)
    output_desc = (IOInfo(V_vec, isotropic),)
    variant_cr = VariantCR(input_desc, output_desc, plapInvariantCR)

    # The experiment doesn't know what inputs this CR takes so it only gives it u. I have to wrap it to get grad(u).
    def ex_plap_cr(u):
        return variant_cr(grad(u))

    ############# Run an experiment to get predicted observations. ############
    tape = get_working_tape()
    with tape.name_scope("Untrained-Run"):
        pred_u = ex.run(ex_plap_cr, u_observer, initial_u=obs_u.copy(deepcopy=True))
        pred = observer(pred_u)
    utils.plot(pred_u, title="Solution with initial network", cmap=EX_CMAP)

    err = loss(obs, pred)
    print("   Initial p: %g" % p)
    print("Initial loss: %g" % err)

    h = Constant(1e-3)
    Jhat = ReducedFunctional(err, Control(p))
    taylor_test(Jhat, p, h)

    if args.opt:
        p_opt = minimize(Jhat, options={"disp": opt_output})
        print("      p_opt: %g" % p_opt)
        print("   opt loss: %g" % Jhat(p_opt))

    utils.showplot()


def make_cr_network(input_spaces, output_space, network, invariant=False):
    inner_cr = CRNetwork(input_spaces, output_space, network)

    if not invariant:
        return inner_cr

    from ufl import Coefficient, FunctionSpace, VectorElement, FiniteElement

    g = Coefficient(FunctionSpace(None, VectorElement(FiniteElement("Real"), dim=2)))
    get_invariant_input = CR_UFL_Expr(UFLExprSpace(g), inner(g, g) + 1e-12, {g: 0})

    cr = get_composite_cr(get_invariant_input, inner_cr)

    def setParams(params):
        inner_cr.setParams(params)
        cr.params = inner_cr.params
        cr.params_controls = inner_cr.params_controls
        cr.network = inner_cr.params[0]

    cr.setParams = setParams
    cr.setParams(inner_cr.params)
    return cr


def run_network_invariant(
    args,
    ex,
    obs,
    obs_u,
    observer,
    loss,
    obsCR,
    network,
    record_stem="",
    opt_output=None,
):
    cr = make_cr_network(input_spaces, output_space, network, invariant=True)
    run_network_cr(
        args, ex, obs, obs_u, observer, loss, obsCR, cr, record_stem, opt_output
    )


def setUpSpace():
    # Create mesh and define function space
    mesh = UnitSquareMesh(6, 6)
    height = 1
    V = FunctionSpace(mesh, "P", 1)

    ############# Define boundary condition. ############
    # u = 0 everywhere except top, where du/dn = 0
    u_D = Constant(0, name="dirichlet_bc")

    def dirichlet_boundary(x, on_boundary):
        return on_boundary and not near(x[1], height, 1e-10)

    bc = DirichletBC(V, u_D, dirichlet_boundary)

    ###### Set up a MeshFunction that marks where to take measurements. ########

    class TopBoundary(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[1], height, 1e-10)

    # Create a function over the boundary mesh, initialized to 0, but set the TopBoundary to 1.
    # These markers mark where we're collecting data.
    markers = MeshFunction("size_t", mesh, mesh.topology().dim() - 1, 0)
    TopBoundary().mark(markers, 1)
    de = ds(1, domain=mesh, subdomain_data=markers)

    return mesh, V, bc, de


def run_taylor_tests(cr, Jhat, mag=1):
    print("Taylor tests:")

    if isinstance(cr, (CRTensorFlow, CRNetwork, CompositePointMap)):
        print("  Taylor test - input")
        h = []
        for a in cr.inputs:
            a_mag = (a._ad_dot(a) + 1e-5) ** 0.5
            vals = a_mag * mag * np.random.rand(a._ad_dim()) * 1e-1
            h_a = a._ad_copy()
            h.append(a._ad_assign_numpy(h_a, vals, 0)[0])
        taylor_test(cr.rf, cr.inputs, h)
    else:
        print("  Taylor test - input")
        u = cr.inputs[0]
        h = Function(u.function_space())
        h.vector()[:] = np.random.rand(h.vector().size())
        uJhat = ReducedFunctional(Jhat.functional, Control(u))
        taylor_test(uJhat, u, h)

    if args.energy_cr:
        return

    print("  Taylor test - param")
    if isinstance(cr, CRNetwork):
        h = cr.network.create_copy("random")._ad_mul(mag)
    elif isinstance(cr, (CRTensorFlow, CompositePointMap)):
        h = [
            create_overloaded_object(mag * np.random.rand(*p.shape)) for p in cr.params
        ]
    else:
        h = Constant(1e-3 * mag)
    taylor_test(Jhat, cr.params, h)


def optimize_params(Jhat, **kwargs):
    record_file = kwargs.pop("record_file", "data.csv")

    # Don't display minimize() info by default.
    if kwargs.get("disp", None) is None:
        kwargs["disp"] = False

    if not record_file:
        minimize_callback = None
    else:
        # Open up the data file and write the column headers.
        with open(record_file, "w") as f:
            f.write("loss,grad_loss\n")

        # Define a function that appends to this data file.
        def minimize_callback(xk, state=None):
            # This callback takes the parameters as a numpy array, so we have to use ReducedFunctionalNumpy.
            Jhat_numpy = ReducedFunctionalNumPy(Jhat)

            # Calculate the loss and loss gradient.
            loss = Jhat_numpy(xk)
            grad_loss = np.linalg.norm(Jhat_numpy.derivative())

            with open(record_file, "a") as f:
                f.write("%g,%g\n" % (loss, grad_loss))

    if kwargs.get("method", "L-BFGS-B") == "L-BFGS-B":
        # Define good defaults for some optimizer parameters.
        kwargs["eps"] = kwargs.get("eps", 1)
        kwargs["gtol"] = kwargs.get("gtol", args.gtol)
        kwargs["ftol"] = kwargs.get("ftol", args.ftol)

    params = minimize(Jhat, options=kwargs, callback=minimize_callback)

    # Pyadjoint bug: minimize doesn't respect the enlistment of Jhat's controls.
    params = Jhat.controls.delist(Enlist(params))
    return params


class CRPlotter:
    def __init__(self, use_u_input=True, mag_radius=5):
        if args.circle:
            self.cr_mesh = UnitDiscMesh.create(MPI.comm_world, 50, 1, 2)
            self.cr_mesh.scale(mag_radius)
        else:
            self.cr_mesh = RectangleMesh(
                Point(-mag_radius, -mag_radius), Point(mag_radius, mag_radius), 100, 100
            )

        scalar_space = FunctionSpace(self.cr_mesh, "P", 1)
        vector_space = VectorFunctionSpace(self.cr_mesh, "P", 1)

        self.vector_quad_space = make_quadrature_spaces(
            (UFLFunctionSpace(vector_space),),
            quad_params=quad_params,
            domain=self.cr_mesh.ufl_domain(),
        )[0][0]
        self.scalar_quad_space = make_quadrature_spaces(
            (UFLFunctionSpace(scalar_space),),
            quad_params=quad_params,
            domain=self.cr_mesh.ufl_domain(),
        )[0][0]

        self.scalar_output_space = scalar_space
        self.vector_output_space = vector_space

        self.use_u_input = use_u_input

        input_spaces = [scalar_space, vector_space]
        input_spaces = tuple(UFLFunctionSpace(s) for s in input_spaces)
        self.input_quad_spaces, self.quad_params = make_quadrature_spaces(
            input_spaces, quad_params=quad_params, domain=self.cr_mesh.ufl_domain()
        )

        self.gradu_np = self.input_quad_spaces[0].tabulate_dof_coordinates()
        self.gradu_mag_np = np.sqrt(
            np.sum(self.gradu_np * self.gradu_np + 1e-12, axis=1)
        )

        if args.vector_cr:
            self.output_space = UFLFunctionSpace(self.input_quad_spaces[1])
        else:
            self.output_space = UFLFunctionSpace(self.input_quad_spaces[0])

        if self.use_u_input:
            self.net_input = [Function(s) for s in self.input_quad_spaces]
            gradu = self.net_input[1]
            self.input_spaces = DirectSum(
                tuple(UFLFunctionSpace(s) for s in self.input_quad_spaces)
            )
        else:
            input_space = self.input_quad_spaces[1]
            gradu = Function(input_space)
            self.net_input = gradu
            self.input_spaces = UFLFunctionSpace(input_space)

        if args.invariant:
            self.input_quad_spaces = list(self.input_quad_spaces)
            self.input_quad_spaces[1] = self.input_quad_spaces[0]

        if not self.use_u_input:
            self.input_quad_spaces = self.input_quad_spaces[1:]

        gradu.vector()[:] = self.gradu_np.flatten()

    def plotPlapCR(self, p, **plot_kwargs):
        plap_cr = Expression(
            "pow(x[0]*x[0] + x[1]*x[1]+1e-12, (p - 2)/2)",
            p=p,
            element=self.scalar_output_space.ufl_element(),
        )
        cmap = plot_kwargs.pop("cmap", GRADU_CMAP)
        return utils.plot(plap_cr, mesh=self.cr_mesh, cmap=cmap, **plot_kwargs)

    def getCROutput(self, cr):
        if hasattr(cr, "network"):
            # Have to recreate the CRNetwork with different input and output spaces.
            # This wouldn't be necessary if Network class worked directly on numpy arrays instead of Functions.
            cr = make_cr_network(
                self.input_quad_spaces,
                self.output_space._functionspace,
                cr.network,
                invariant=args.invariant,
            )

        ufl_cr = get_composite_cr(
            self.input_spaces,
            cr,
            self.output_space,
            domain=self.cr_mesh.ufl_domain(),
            quad_params=self.quad_params,
        )
        out_func = ufl_cr(self.net_input)
        return out_func

    def getCROutputXY(self, cr, u):
        gradu = grad(u)
        if self.use_u_input:
            inputs = [u, gradu]
            input_spaces = DirectSum(
                tuple(UFLExprSpace(f, ufl_domains=()) for f in inputs)
            )
        else:
            inputs = gradu
            input_spaces = UFLExprSpace(inputs, ufl_domains=())

        gradu_func = project(gradu, vector_output_space)
        gradu_mag_func = project(sqrt(inner(gradu, gradu) + 1e-12), scalar_output_space)

        ufl_cr = get_composite_cr(input_spaces, cr, cr_output_spaces)
        out_func = ufl_cr(inputs)
        if out_func.ufl_shape == ():
            out_func = project(out_func, scalar_output_space)
        else:
            out_func = project(out_func, vector_output_space)

        return out_func, gradu_func, gradu_mag_func

    def plot1D(
        self, out_func, gradu_mag_xy, out_func_xy, ptrue, title="", **plot_kwargs
    ):
        markersize = plot_kwargs.pop("markersize", 10)

        out_np = out_func.vector()[:].reshape(-1, *out_func.ufl_shape)
        out_np_xy = out_func_xy.vector()[:].reshape(-1, *out_func_xy.ufl_shape)
        gradu_mag_np_xy = gradu_mag_xy.vector()[:].reshape(-1, *gradu_mag_xy.ufl_shape)

        true_out_np = self.gradu_mag_np ** (ptrue - 2)
        true_out_np_xy = gradu_mag_np_xy ** (ptrue - 2)

        (exp_line,) = plt.plot(
            self.gradu_mag_np,
            out_np,
            ".",
            alpha=0.01,
            **plot_kwargs,
            label="Experiment CR"
        )
        plt.plot(self.gradu_mag_np, true_out_np, "-", **plot_kwargs, label="True CR")

        plot_kwargs["markersize"] = markersize
        plt.plot(
            gradu_mag_np_xy,
            out_np_xy,
            "x",
            c="r",
            **plot_kwargs,
            label="Experiment CR (visible)"
        )
        plt.plot(
            gradu_mag_np_xy,
            true_out_np_xy,
            "+",
            c="g",
            **plot_kwargs,
            label="True CR (visible)"
        )
        plt.title(title)

        from matplotlib.legend_handler import HandlerLine2D

        plt.legend(handler_map={exp_line: HandlerLine2D(numpoints=100)})

        plt.xlabel(r"$\sqrt{||\nabla u||^2 + 10^{-12}}$")
        plt.ylabel(r"$cr(\nabla u)$")

        utils.saveplot()
        plt.figure()

    def plot3D(
        self,
        out_func,
        gradu_xy,
        gradu_mag_xy,
        out_func_xy,
        ptrue,
        title="",
        **plot_kwargs
    ):
        out_np = out_func.vector()[:].reshape(-1, *out_func.ufl_shape)
        out_np_xy = out_func_xy.vector()[:].reshape(-1, *out_func_xy.ufl_shape)
        gradu_np_xy = gradu_xy.vector()[:].reshape(-1, *gradu_xy.ufl_shape)
        gradu_mag_np_xy = gradu_mag_xy.vector()[:].reshape(-1, *gradu_mag_xy.ufl_shape)

        if len(out_np.shape) <= 1:
            out_np = out_np[:, None]
        if len(out_np_xy.shape) <= 1:
            out_np_xy = out_np_xy[:, None]

        true_out_np = self.gradu_mag_np ** (ptrue - 2)
        true_out_np_xy = gradu_mag_np_xy ** (ptrue - 2)

        if len(true_out_np.shape) <= 1:
            true_out_np = true_out_np[:, None]
        if len(true_out_np_xy.shape) <= 1:
            true_out_np_xy = true_out_np_xy[:, None]

        from mpl_toolkits.mplot3d import Axes3D

        ax = plt.gcf().add_subplot(111, projection="3d")

        ax.plot_wireframe(
            self.gradu_np[:, :1],
            self.gradu_np[:, 1:],
            out_np,
            linewidths=0.5,
            linestyles="dashed",
            colors="#d95f02",
            **plot_kwargs,
            label="Experiment CR"
        )
        ax.plot_wireframe(
            self.gradu_np[:, :1],
            self.gradu_np[:, 1:],
            true_out_np,
            linewidths=0.5,
            colors="#1b9e77",
            **plot_kwargs,
            label="True CR"
        )

        plot_kwargs["s"] = plot_kwargs.get("s", 50)
        ax.scatter(
            gradu_np_xy[:, 0],
            gradu_np_xy[:, 1],
            out_np_xy,
            marker="x",
            c="r",
            **plot_kwargs,
            label="Experiment CR (visible)"
        )
        ax.scatter(
            gradu_np_xy[:, 0],
            gradu_np_xy[:, 1],
            true_out_np_xy,
            marker="+",
            c="g",
            **plot_kwargs,
            label="True CR (visible)"
        )
        plt.title(title)
        plt.legend()

        utils.saveplot()
        plt.figure()

    def plotNetworkCR(self, out_func, **plot_kwargs):
        cmap = plot_kwargs.pop("cmap", GRADU_CMAP)
        if out_func.ufl_shape == ():
            out_func = project(out_func, self.scalar_output_space)
        else:
            out_func = project(out_func, self.vector_output_space)
        return utils.plot(out_func, cmap=cmap, **plot_kwargs)

    def plotXYCR(self, out_func_xy, **plot_kwargs):
        cmap = plot_kwargs.pop("cmap", XY_CMAP)
        return utils.plot(out_func_xy, cmap=cmap, **plot_kwargs)


def run_p_laplacian_cr(args, ex, obs, observer, loss, record_stem="", opt_output=None):
    ############# Run an experiment to get predicted observations. ############
    #############   Note: Different CR parameters are used.        ####
    #############         The goal will be to figure out the real  ####
    #############         p starting out at a wrong p.             ####
    cr = CR_P_Laplacian(p=Constant(args.startp, name="p2"), input_u=args.input_u)

    tape = get_working_tape()
    with tape.name_scope("Untrained-Run"):
        pred_u = ex.run(cr, u_observer)
        pred = observer(pred_u)
    utils.plot(
        pred_u.copy(deepcopy=True, annotate=False),
        title="Solution with p = %g" % cr._p,
        cmap=EX_CMAP,
    )

    ############# Calculate loss and gradient of loss wrt p and u. ############
    err = loss(obs, pred)
    print("   Initial p: %g" % cr._p)
    print("Initial loss: %g" % err)

    Jhat = ReducedFunctional(err, Control(cr._p))
    # run_taylor_tests(cr, Jhat)

    ############# Optimize ##################
    if args.opt:
        print()
        print("Optimizing...")
        new_params = optimize_params(
            Jhat, record_file="%s.csv" % record_stem, disp=opt_output
        )

        err = Jhat(new_params)
        print("   Final p: %g" % new_params)
        print("Final loss: %g" % err)

        new_pred_u = Control(pred_u).tape_value()
        utils.plot(
            new_pred_u.copy(deepcopy=True, annotate=False),
            title="Solution with p_opt = %g" % new_params,
            cmap=EX_CMAP,
        )

    utils.showplot()


def run_network_cr(
    args, ex, obs, obs_u, observer, loss, obsCR, cr, record_stem="", opt_output=None
):
    if hasattr(cr, "network"):
        print(cr.network.summary())

    ############# Run an experiment to get predicted observations. ############
    tape = get_working_tape()
    with tape.name_scope("Untrained-Run"):
        pred_u = ex.run(cr, u_observer, initial_u=obs_u.copy(deepcopy=True))
        pred = observer(pred_u)
        experiment_rf = ReducedFunction(pred_u, cr.params_controls)

    utils.plot(pred_u, title="Solution with initial network", cmap=EX_CMAP)

    ############# Calculate loss and gradient of loss wrt p and u. ############
    err = loss(obs, pred)
    print("Initial loss: %g" % err)
    print()

    Jhat = ReducedFunctional(err, cr.params_controls)
    Jhat(cr.params)
    # run_taylor_tests(cr, Jhat)

    with stop_annotating():
        res = ex.get_residual(cr, pred_u)
    utils.plot(res, title="Residual", cmap=EX_CMAP)

    cr_plotter = CRPlotter(use_u_input=args.input_u)
    with stop_annotating():
        # Plot the CR as a function of gradu.
        cr_gradu_cb = cr_plotter.plotPlapCR(
            obsCR._p, title="Correct CR: f(grad_u) (standard color)"
        )
        out_func = cr_plotter.getCROutput(cr)
        cr_plotter.plotNetworkCR(
            out_func,
            title="Untrained CR: f(grad_u) (standard color)",
            vmin=cr_gradu_cb.zmin,
            vmax=cr_gradu_cb.zmax,
        )
        cr_plotter.plotNetworkCR(out_func, title="Untrained CR: f(grad_u)")

        # Plot the CR as a function of x and y.
        # Do some trickery to get the scalar factor of the CR.
        gradu = grad(obs_u)
        if args.input_u:
            inputs = (obs_u, gradu)
        else:
            inputs = gradu
        correctCR_output = project(
            dot(obsCR(inputs), gradu) / (inner(gradu, gradu) + 1e-12), ex.V
        )
        cr_xy_cb = cr_plotter.plotXYCR(
            correctCR_output, title="Correct CR: f(x, y) (standard color)"
        )

        out_func_xy, gradu_xy, gradu_mag_xy = cr_plotter.getCROutputXY(cr, obs_u)
        cr_plotter.plotXYCR(out_func_xy, title="Untrained CR: f(x, y)")
        cr_plotter.plotXYCR(
            out_func_xy,
            title="Untrained CR: f(x, y) (standard color)",
            vmin=cr_xy_cb.zmin,
            vmax=cr_xy_cb.zmax,
        )

        cr_plotter.plot1D(
            out_func,
            gradu_mag_xy,
            out_func_xy,
            ptrue=args.ptrue,
            title="Untrained CR: f(||grad_u||)",
        )
        cr_plotter.plot3D(
            out_func,
            gradu_xy,
            gradu_mag_xy,
            out_func_xy,
            ptrue=args.ptrue,
            title="Untrained CR: f(grad_u) 3D",
        )

    ############# Optimize ##################
    if args.opt:
        print()
        print("Optimizing...")

        # For the optimization, I'll do it only using the blocks that were already recorded on the tape.
        new_params = optimize_params(
            Jhat, record_file="%s.csv" % record_stem, disp=opt_output
        )

        # I'll use the experiment reduced function to rerun the experiment and the loss function to
        # recompute the error.
        err = Jhat(new_params)
        print("Final loss: %g" % err)
        new_pred_u = Control(pred_u).tape_value()
        utils.plot(new_pred_u, title="Solution with optimized network 1", cmap=EX_CMAP)

        if hasattr(cr, "network") and hasattr(cr.network, "_p"):
            print("Final p value:", cr.network._p)

        cr.setParams(new_params)

        with stop_annotating():
            # Plot the CR as a function of gradu.
            out_func = cr_plotter.getCROutput(cr)
            cr_plotter.plotNetworkCR(
                out_func,
                title="Trained CR 1: f(grad_u) (standard color)",
                vmin=cr_gradu_cb.zmin,
                vmax=cr_gradu_cb.zmax,
            )
            cr_plotter.plotNetworkCR(out_func, title="Trained CR 1: f(grad_u)")

            # Plot the CR as a function of x and y.
            out_func_xy, gradu_xy, gradu_mag_xy = cr_plotter.getCROutputXY(cr, obs_u)
            cr_plotter.plotXYCR(out_func_xy, title="Trained CR: f(x, y)")
            cr_plotter.plotXYCR(
                out_func_xy,
                title="Trained CR: f(x, y) (standard color)",
                vmin=cr_xy_cb.zmin,
                vmax=cr_xy_cb.zmax,
            )

            cr_plotter.plot1D(
                out_func,
                gradu_mag_xy,
                out_func_xy,
                ptrue=args.ptrue,
                title="Trained CR: f(||grad_u||)",
            )
            cr_plotter.plot3D(
                out_func,
                gradu_xy,
                gradu_mag_xy,
                out_func_xy,
                ptrue=args.ptrue,
                title="Trained CR: f(grad_u) 3D",
            )

    utils.showplot()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    utils.add_bool_arg(parser, "opt", False, "Run gradient descent")
    utils.add_bool_arg(
        parser, "viz_tape", False, "Save dolfin-adjoint tape for visualization"
    )
    utils.add_bool_arg(parser, "show", True, "whether to show plots interactively")
    utils.add_bool_arg(
        parser, "opt_output", False, "show optimization iteration output"
    )
    utils.add_bool_arg(parser, "circle", False, "use a circular mesh for the CR plot.")

    utils.add_bool_arg(parser, "input_u", False, "whether the network takes u as input")
    parser.add_argument(
        "--cr",
        type=str,
        choices=["scalar", "vector", "energy"],
        help="type of cr.",
        default="scalar",
    )

    parser.add_argument("--seed", type=int, help="seed for RNG", default=None)
    parser.add_argument(
        "-p", "--ptrue", type=float, help="true value to use for p", default=2.5
    )
    parser.add_argument(
        "--startp", type=float, help="initial value to use for p", default=2.1
    )
    parser.add_argument(
        "--gtol",
        type=float,
        help="optimization stop condition for gradient",
        default=1e-10,
    )
    parser.add_argument(
        "--ftol",
        type=float,
        help="optimization stop condition for decrease in residual",
        default=1e-10,
    )
    parser.add_argument(
        "-r",
        "--record_stem",
        type=str,
        help="file stem to record convergence during optimization",
        default="data",
    )
    parser.add_argument(
        "-s", "--save_dir", type=str, help="directory to save figures to", default=""
    )
    parser.add_argument(
        "-f",
        "--format",
        type=str,
        help="image format to use for figures",
        default="png",
    )

    parser.add_argument(
        "--ex_cmap", type=str, help="color map for experiment output", default="magma_r"
    )
    parser.add_argument(
        "--gradu_cmap",
        type=str,
        help="color map for CR(grad_u) plots",
        default="spring",
    )
    parser.add_argument(
        "--xy_cmap", type=str, help="color map for CR(x, y) plots", default="jet"
    )

    parser.add_argument(
        "-l",
        "--loss",
        type=str,
        choices=["integral", "vector"],
        help="loss function to use.",
        default="integral",
    )
    parser.add_argument(
        "-o",
        "--observer",
        type=str,
        choices=["u", "surface"],
        help="observer function to use",
        default="u",
    )

    modes = [
        "plap",
        "plap_network",
        "network",
        "plap_tensorflow",
        "network_tensorflow",
        "network_invariant",
        "network_tensorflow_invariant",
    ]
    parser.add_argument(
        "mode", choices=modes, help="whether to use network for CR or p-Laplacian"
    )

    args = parser.parse_args()

    args.invariant = "invariant" in args.mode

    # Don't print out Newton iterations.
    set_log_level(LogLevel.CRITICAL)

    # Handle the plotting arguments.
    utils.SHOW_FIGURES = args.show
    if args.save_dir != "":
        utils.FIGURE_SAVE_DIR = args.save_dir
        try:
            import matplotlib.pyplot as plt

            plt.rc("savefig", format=args.format)
        except Exception as e:
            print("Warning: couldn't set output image format:", e)

    np.random.seed(args.seed)

    try:
        from matplotlib import cm

        EX_CMAP = cm.get_cmap(args.ex_cmap)
        GRADU_CMAP = cm.get_cmap(args.gradu_cmap)
        XY_CMAP = cm.get_cmap(args.xy_cmap)
    except Exception as e:
        print("Warning: couldn't get matplotlib colormap:", e)

    mesh, V, bc, de = setUpSpace()
    quad_params = {
        "quadrature_degree": 4,
        "quadrature_rule": "default",
        "representation": "quadrature",
    }
    set_default_covering_params(domain=mesh.ufl_domain(), quad_params=quad_params)
    V_vec = VectorFunctionSpace(mesh, "CG", 1)
    input_spaces = [V, V_vec]
    scalar_output_space = V
    vector_output_space = V_vec

    if args.cr == "energy":
        if args.mode not in ["plap_tensorflow", "network_tensorflow"]:
            print(
                "Error: energy cr is only implemented for plap_tensorflow and network_tensorflow"
            )
            sys.exit(1)
        if args.opt:
            print("Error: energy cr cannot be used with the opt option yet")
            sys.exit(1)

    if args.input_u:
        # TODO: check on this. I think the FEniCS quadrature bug related to this has been fixed.
        print("Error: cannot use input_u yet")
        sys.exit(1)

    args.vector_cr = args.cr == "vector"
    args.energy_cr = args.cr == "energy"
    output_space = vector_output_space if args.vector_cr else scalar_output_space

    if args.invariant and args.vector_cr:
        print("Error: the invariant form cannot output a vector yet")
        sys.exit(1)

    if args.invariant:
        # We'll be inputting a scalar function instead of a vector function.
        input_spaces[1] = input_spaces[0]

    if not args.input_u:
        input_spaces = input_spaces[1:]

    cr_input_spaces = DirectSum(
        [
            UFLExprSpace(None, input_space.ufl_element().value_shape(), [])
            for input_space in input_spaces
        ]
    )
    if len(input_spaces) == 1:
        cr_input_spaces = cr_input_spaces[0]
    cr_output_spaces = UFLExprSpace(None, output_space.ufl_element().value_shape(), [])

    ############# Define CR, with some initial value for p. ############
    cr_p_laplacian = CR_P_Laplacian(
        p=Constant(args.ptrue, name="p"), input_u=args.input_u
    )

    ############# Create experiment. ##########
    alpha = 20 * pi / 180
    ex = Experiment(alpha=alpha, input_u=args.input_u)
    ex.setDomainParameters(V, bc)

    if args.observer == "u":
        observer = u_observer
    else:
        observer = SurfaceObserver(de)

    if args.loss == "integral":
        loss = integral_loss
    else:
        if observer != u_observer:
            print("Error: vector loss can only be used with u observer")
            sys.exit(1)
        loss = vector_loss

    ############# Run initial experiment to get base observations. ############
    tape = get_working_tape()
    with tape.name_scope("Ground-truth"):
        obs_u = ex.run(cr_p_laplacian, u_observer, ufl=True)
        obs = observer(obs_u)
    utils.plot(obs_u, title="Solution with p = %g" % cr_p_laplacian._p, cmap=EX_CMAP)

    if args.mode == "plap":
        run_p_laplacian_cr(
            args,
            ex,
            obs,
            observer,
            loss,
            record_stem=args.record_stem,
            opt_output=args.opt_output,
        )
    elif args.mode == "network":
        # Create enough inputs for grad(u) and enough outputs for the vector sigma.
        num_inputs, num_outputs = get_num_network_inputs_outputs(
            output_space, *input_spaces
        )
        output_activation = "linear" if args.vector_cr else "softplus"
        layers = Network.create_layers(
            num_inputs, num_outputs, output_activation, [40, 30, 20], "tanh"
        )

        network = Network(layers)
        run_network(
            args,
            ex,
            obs,
            obs_u,
            observer,
            loss,
            cr_p_laplacian,
            network,
            record_stem=args.record_stem,
            opt_output=args.opt_output,
        )
    elif args.mode == "plap_network":
        plap_network = PlapNetwork(
            p=args.startp,
            dims=mesh.geometric_dimension(),
            input_just_vector=not args.input_u,
            output_vector=args.vector_cr,
        )
        run_network(
            args,
            ex,
            obs,
            obs_u,
            observer,
            loss,
            cr_p_laplacian,
            plap_network,
            record_stem=args.record_stem,
            opt_output=args.opt_output,
        )
    elif args.mode == "network_tensorflow_invariant":
        run_network_tensorflow_invariant(
            args,
            ex,
            obs,
            obs_u,
            observer,
            loss,
            cr_p_laplacian,
            record_stem=args.record_stem,
            opt_output=args.opt_output,
        )
    elif args.mode == "network_invariant":
        # Create enough inputs for grad(u) and enough outputs for the vector sigma.
        num_inputs, num_outputs = get_num_network_inputs_outputs(
            output_space, *input_spaces
        )
        output_activation = "linear" if args.vector_cr else "softplus"
        layers = Network.create_layers(
            num_inputs, num_outputs, output_activation, [40, 30, 20], "tanh"
        )

        network = Network(layers)
        run_network_invariant(
            args,
            ex,
            obs,
            obs_u,
            observer,
            loss,
            cr_p_laplacian,
            network,
            record_stem=args.record_stem,
            opt_output=args.opt_output,
        )
    # elif args.mode == 'plap_invariant':
    #     run_plap_invariant(args, ex, obs, obs_u, observer, loss, cr_p_laplacian,
    #         record_stem=args.record_stem, opt_output=args.opt_output)
    elif args.mode == "plap_tensorflow":
        run_plap_tensorflow_jacobian(
            args,
            ex,
            obs,
            obs_u,
            observer,
            loss,
            cr_p_laplacian,
            record_stem=args.record_stem,
            opt_output=args.opt_output,
        )
    elif args.mode == "network_tensorflow":
        run_network_tensorflow_jacobian(
            args,
            ex,
            obs,
            obs_u,
            observer,
            loss,
            cr_p_laplacian,
            record_stem=args.record_stem,
            opt_output=args.opt_output,
        )
    else:
        print("Internal error: invalid mode  '%s'" % args.mode)
        sys.exit(1)

    ############# Visualize everything I did. ##############
    if args.viz_tape:
        get_working_tape().visualise(launch_tensorboard=True, open_in_browser=True)
