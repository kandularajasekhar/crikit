from crikit import assemble_with_cr, create_ufl_standins, UFLExprSpace, UFLFunctionSpace
from crikit.fe import *
from crikit.fe_adjoint import *
from pyadjoint.enlisting import Enlist
from pyadjoint_utils import *


class Experiment:
    def __init__(self, mesh, quad_params, alpha=0, g=9.8, rho=1):
        self.setDomainParameters(mesh, quad_params)
        self.setF(alpha=alpha, g=g, rho=rho)

    def setF(self, alpha=None, g=None, rho=None):
        self.alpha = self.alpha if alpha is None else alpha
        self.g = self.g if g is None else g
        self.rho = self.rho if rho is None else rho
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        f = (
            -self.rho * self.g * sin(self.alpha),
            -self.rho * self.g * cos(self.alpha),
            0,
        )
        if self.dim == 2:
            f = f[:2]
        self.f = Constant(f, name="f")

    def setDomainParameters(self, mesh, quad_params):
        V_e = VectorElement("CG", mesh.ufl_cell(), 2)
        Q_e = FiniteElement("CG", mesh.ufl_cell(), 1)
        VQ_e = V_e * Q_e
        self.W = FunctionSpace(mesh, VQ_e)
        self.dim = V_e.value_shape()[0]
        self.mesh = mesh
        self.quad_params = quad_params

    def setBCs(self, dir_bcs, rob_bcs=None):
        """
        Sets Dirichlet and Robin boundary conditions. Each Robin condition is a
        tuple (a, b, j, ds) corresponding to the equation :math:`a u + b mu sym(grad(u)) n = j`
        on the boundary ds. Note that the Robin conditions are only applied
        to the velocity, not the pressure.

        Args:
            dir_bcs (DirichletBC or list[DirichletBC]): The Dirichlet boundary conditions.
            rob_bcs (tuple or list[tuple]): See description above.
        """
        self.bcs = Enlist(dir_bcs)
        self.h_bcs = homogenize_bcs(self.bcs)
        self.rob_bcs = Enlist(rob_bcs) if rob_bcs is not None else None

    def bc(self, u):
        """Applies Dirichlet boundary conditions to function or to matrix"""
        if hasattr(u, "vector"):
            for bc in self.bcs:
                bc.apply(u.vector())
        else:
            for bc in self.bcs:
                bc.apply(u)
        return u

    def h_bc(self, u):
        """Applies homogenous Dirichlet boundary conditions to function or to matrix"""
        for bc in self.h_bcs:
            bc.apply(u.vector())
        return u

    def get_robin_terms(self, u, v):
        """Adds the Robin terms to the given Form"""
        if self.rob_bcs is None:
            return 0
        F = 0
        for a, b, j, ds in self.rob_bcs:
            F += inner(v, (a * u - j)) / b * ds
        return F

    def run(
        self,
        cr,
        observer=None,
        initial_w=None,
        ufl=False,
        cback=None,
        solver_parameters=None,
        disp=True,
    ):
        if solver_parameters is None:
            solver_parameters = {}

        # Define u.
        if initial_w is None:
            w = Function(self.W, name="w")
        else:
            w = initial_w

        if ufl:
            w = self._ufl_solve(cr, w, cback=cback, disp=disp)
        else:
            w = self._solve(cr, w, cback=cback, disp=disp)
        if observer is None:
            return w
        return observer(w)

    def _ufl_solve(self, cr, w, cback=None, disp=True):
        # Don't print out Newton iterations.
        if not disp:
            orig_log_level = get_log_level()
            set_log_level(LogLevel.CRITICAL)

        # Set up the weak form.
        u, p = split(w)
        sigma = cr(sym(grad(u)))
        if sigma.ufl_shape == ():
            sigma = sigma * sym(grad(u))
        F, u = self.get_form(w, sigma)

        solve(F == 0, w, self.bcs)

        if not disp:
            set_log_level(orig_log_level)

        if cback is not None:
            cback(w)
        return w

    def _solve(self, cr, w, cback=None, disp=True):
        # Set up the weak form.
        F, u, cr_output = self.get_form_cr(cr, w)

        with push_tape():
            residual = Function(self.W)
            assemble_with_cr(
                F,
                cr,
                sym(grad(u)),
                cr_output,
                tensor=residual,
                quad_params=self.quad_params,
            )

            wcontrol = Control(w)
            residual_rf = ReducedFunction(residual, wcontrol)
        reduced_equation = ReducedEquation(residual_rf, self.bc, self.h_bc)

        solver = SNESSolver(reduced_equation, {"jmat_type": "assembled"})
        w = solver.solve(wcontrol, disp=disp, cback=cback)
        return w

    def get_form_cr(self, cr, w):
        target_shape = tuple(i for i in cr.target.shape() if i != -1)
        cr_output = create_ufl_standins((target_shape,))[0]
        u, p = split(w)
        sigma = cr_output * sym(grad(u)) if cr_output.ufl_shape == () else cr_output
        F, u = self.get_form(w, sigma)
        return F, u, cr_output

    def get_form(self, w, sigma):
        u, p = split(w)
        w_test = TestFunction(self.W)
        v, q = split(w_test)

        lhs = inner(sigma, grad(v)) * dx - inner(div(v), p) * dx - inner(div(u), q) * dx
        rhs = inner(self.f, v) * dx
        F = lhs - rhs
        F = F + self.get_robin_terms(u, v)
        return F, u
