from crikit.cr import JAXArrays
from crikit.cr.quadrature import make_quadrature_spaces
from crikit.cr.ufl import UFLExprSpace, UFLFunctionSpace
from crikit.covering import get_composite_cr, get_default_covering_params
from crikit.fe import *
from crikit.fe_adjoint import *
from pyadjoint import *
from pyadjoint_utils.jax_adjoint import overload_jax
from pyadjoint_utils import *

from crikit.projection import project

import crikit.utils as utils
import numpy as onp

try:
    import matplotlib.pyplot as plt
except:
    pass


def get_dx(el, dx):
    if el.family() == "Quadrature":
        quad_params = {
            "quadrature_rule": el.quadrature_scheme(),
            "quadrature_degree": el.degree(),
            "representation": "quadrature",
        }
        dx = dx(metadata=quad_params)
    return dx


class InvariantCRPlotter:
    def __init__(
        self,
        xy_mesh,
        p,
        eps2,
        quad_params=None,
        x_range=[-0.4, 0.4],
        y_range=[0, 3],
        energy=True,
        INVARIANT_CMAP=None,
        XY_CMAP=None,
    ):
        self.p = p
        self.eps2 = eps2
        self.INVARIANT_CMAP = INVARIANT_CMAP
        self.XY_CMAP = XY_CMAP
        self._energy = energy
        self.dim = xy_mesh.geometric_dimension()

        self.cr_mesh = RectangleMesh(
            Point(x_range[0], y_range[0]), Point(x_range[1], y_range[1]), 10, 10
        )
        self.aspect_ratio = (y_range[1] - y_range[0]) / (x_range[1] - x_range[0])
        self.quad_params = (
            quad_params
            if quad_params is not None
            else get_default_covering_params().get("quad_params", None)
        )
        degree = min(self.quad_params.get("quadrature_degree", 1), 3)

        self.input_space = VectorFunctionSpace(self.cr_mesh, "P", degree)
        self.scalar_space = FunctionSpace(self.cr_mesh, "P", degree)
        if self._energy:
            self.output_space = FunctionSpace(self.cr_mesh, "P", degree)
        else:
            self.output_space = VectorFunctionSpace(
                self.cr_mesh, "P", degree, dim=self.dim
            )

        spaces = [self.input_space, self.scalar_space, self.output_space]
        spaces = tuple(UFLFunctionSpace(s) for s in spaces)
        self.quad_spaces, self.quad_params = make_quadrature_spaces(
            spaces, quad_params=self.quad_params, domain=self.cr_mesh.ufl_domain()
        )
        self.input_space_quad, self.scalar_space_quad, self.output_space_quad = (
            UFLFunctionSpace(Q) for Q in self.quad_spaces
        )

        self.cr_input_func = Function(self.input_space_quad._functionspace)
        self.input_np = self.scalar_space_quad._functionspace.tabulate_dof_coordinates()
        self.cr_input_func.vector()[:] = self.input_np.flatten()

        self.xy_mesh = xy_mesh
        tensor_space_xy = TensorFunctionSpace(
            self.xy_mesh, "P", degree, shape=(self.dim, self.dim)
        )
        input_space_xy = VectorFunctionSpace(self.xy_mesh, "P", degree)
        self.scalar_space_xy = FunctionSpace(self.xy_mesh, "P", degree)
        if self._energy:
            output_space_xy = FunctionSpace(self.xy_mesh, "P", degree)
        else:
            output_space_xy = VectorFunctionSpace(
                self.xy_mesh, "P", degree, dim=self.dim
            )
        spaces = [tensor_space_xy, input_space_xy, output_space_xy]
        spaces = tuple(UFLFunctionSpace(s) for s in spaces)
        quad_spaces, _ = make_quadrature_spaces(
            spaces, quad_params=self.quad_params, domain=self.xy_mesh.ufl_domain()
        )
        self.tensor_space_xy, self.input_space_xy, self.output_space_xy = (
            UFLFunctionSpace(Q) for Q in quad_spaces
        )

    def plot_correct_all(self, u):
        # Plot the CR as a function of x and y in the experiment.
        strain = sym(grad(u))
        i2 = inner(strain, strain) + self.eps2
        if self._energy:
            correct_out = 1 / self.p * i2 ** (self.p / 2)
        else:
            correct_out = i2 ** ((self.p - 2) / 2)

        correct_out_func_xy = project(correct_out, self.scalar_space_xy)

        plt.figure()
        true_cb_xy = self.plot_cr_output_xy(
            correct_out_func_xy, title="Correct CR: f(x, y) (standard color)"
        )
        utils.saveplot()

        plt.figure()
        true_cb = self.plot_power_law(
            title="Correct CR: f(invariants) (standard color)"
        )
        utils.saveplot()

        return true_cb, true_cb_xy

    def plot_cr_all(
        self, cr, params, strain, true_cb, true_cb_xy, net_eval_cr=None, prefix=""
    ):
        out_func_xy_quad, invariants_xy_np, energy_xy_np = self.run_cr_xy(
            cr, params, strain
        )

        plt.figure()
        self.plot_cr_output_xy(out_func_xy_quad, title=f"{prefix}f(x, y)")
        utils.saveplot()

        plt.figure()
        self.plot_cr_output_xy(
            out_func_xy_quad,
            title=f"{prefix}f(x, y) (standard color)",
            vmin=true_cb_xy.zmin,
            vmax=true_cb_xy.zmax,
        )
        utils.saveplot()

        # Plot the CR as a function of the invariants.
        out_func = self.run_cr(net_eval_cr)

        plt.figure()
        self.plot_cr_output(
            out_func,
            invariants_xy_np,
            title=f"{prefix}f(invariants) (standard color)",
            vmin=true_cb.zmin,
            vmax=true_cb.zmax,
        )
        utils.saveplot()

        plt.figure()
        self.plot_cr_output(out_func, invariants_xy_np, title=f"{prefix}f(invariants)")
        utils.saveplot()

        plt.figure()
        self.plot_1d(
            out_func, invariants_xy_np, energy_xy_np, title=f"{prefix}f(invariants[1])"
        )
        utils.saveplot()

    def run_cr(self, cr):
        ufl_cr = get_composite_cr(
            self.input_space_quad,
            cr,
            self.output_space_quad,
            domain=self.cr_mesh.ufl_domain(),
            quad_params=self.quad_params,
        )
        out_func = ufl_cr(self.cr_input_func)
        return out_func

    def run_cr_xy(self, cr, params, strain):
        input_spaces = UFLExprSpace(strain, ufl_domains=())

        tensor_projection = get_composite_cr(
            input_spaces, self.tensor_space_xy, JAXArrays((-1, 2, 2))
        )
        strain_np = tensor_projection(strain)

        overloaded_scalar_invt_func = overload_jax(cr._scalar_invt_func, nojit=False)
        overloaded_f = overload_jax(cr._f, nojit=False)
        invariants_np = overloaded_scalar_invt_func(strain_np)
        output_np = overloaded_f(invariants_np, *params)

        output_to_quad_space = get_composite_cr(
            JAXArrays((-1,) + output_np.shape[1:]), self.output_space_xy
        )
        out_func = output_to_quad_space(output_np)

        return out_func, invariants_np, output_np

    def plot_power_law(self, **plot_kwargs):
        cmap = plot_kwargs.pop("cmap", self.INVARIANT_CMAP)
        if self._energy:
            plap_cr = Expression(
                "1.0 / p * pow(x[1] + eps2, p/2.0)",
                p=self.p,
                eps2=self.eps2,
                element=self.output_space.ufl_element(),
            )
            f = utils.plot(
                plap_cr, mesh=self.cr_mesh, cmap=cmap, newfig=False, **plot_kwargs
            )
        else:
            # Plot only the coefficient for the epsilon term.
            plap_cr = Expression(
                "pow(x[1] + eps2, (p-2.0)/2.0)",
                p=self.p,
                eps2=self.eps2,
                element=self.output_space.ufl_element(),
            )
            f = utils.plot(
                plap_cr, mesh=self.cr_mesh, cmap=cmap, newfig=False, **plot_kwargs
            )
        plt.xlabel(r"tr $\epsilon$")
        plt.ylabel(r"tr $\epsilon^2$")
        plt.gca().set_aspect(1 / self.aspect_ratio)
        return f

    def plot_cr_output(self, out_func_quad, invariants_xy_np=None, **plot_kwargs):
        cmap = plot_kwargs.pop("cmap", self.INVARIANT_CMAP)
        s = plot_kwargs.pop("s", 50)

        if out_func_quad is not None:
            if self._energy:
                data = out_func_quad.vector()[:]
                out_func_quad.vector()[:] = data - onp.min(data)
                out_func = project(out_func_quad, self.output_space)
                cb = utils.plot(out_func, cmap=cmap, newfig=False, **plot_kwargs)
                out_func_quad.vector()[:] = data
            else:
                quad_dx = get_dx(out_func_quad.ufl_element(), dx)
                out_func = project(out_func_quad[1], self.scalar_space, dx=quad_dx)
                cb = utils.plot(out_func, cmap=cmap, newfig=False, **plot_kwargs)
        else:
            cb = None
        if invariants_xy_np is not None:
            plt.scatter(
                invariants_xy_np[:, 0],
                invariants_xy_np[:, 1],
                marker="x",
                c="r",
                alpha=1,
                s=s,
                label="Experiment Invariants",
            )
            plt.legend()
        plt.xlabel(r"tr $\epsilon$")
        plt.ylabel(r"tr $\epsilon^2$")
        plt.gca().set_aspect(1 / self.aspect_ratio)
        return cb

    def plot_cr_output_xy(self, out_func_xy, **plot_kwargs):
        cmap = plot_kwargs.pop("cmap", self.XY_CMAP)
        quad_dx = get_dx(out_func_xy.ufl_element(), dx)
        if out_func_xy.ufl_shape == ():
            out_func_xy = project(out_func_xy, self.scalar_space_xy, dx=quad_dx)
            cb = utils.plot(out_func_xy, cmap=cmap, newfig=False, **plot_kwargs)
        else:
            out_func_xy = project(out_func_xy[1], self.scalar_space_xy, dx=quad_dx)
            cb = utils.plot(out_func_xy, cmap=cmap, newfig=False, **plot_kwargs)
        return cb

    def plot_1d(
        self, out_func, invariants_xy_np, pred_out_np_xy, title="", **plot_kwargs
    ):
        out_np = (
            out_func.vector()[:].reshape(-1, *out_func.ufl_shape)
            if out_func is not None
            else 0
        )

        x_data = self.input_np[:, 1]
        x_data_xy = invariants_xy_np[:, 1]

        sorted_ids = onp.argsort(x_data)
        x_data = x_data[sorted_ids]
        out_np = out_np[sorted_ids] if out_func is not None else 0

        sorted_ids = onp.argsort(x_data_xy)
        x_data_xy = x_data_xy[sorted_ids]
        pred_out_np_xy = pred_out_np_xy[sorted_ids]

        if self._energy:

            def true_func(x):
                return 1 / self.p * (x + self.eps2) ** (self.p / 2)

        else:

            def true_func(x):
                return (x + self.eps2) ** ((self.p - 2) / 2)

        true_out_np = true_func(x_data)
        true_out_np_xy = true_func(x_data_xy)

        if self._energy:
            # The energy may be offset by a constant, so to compare them, I'm modifying the prediction to have the same offset.
            energy_offset = onp.mean(true_out_np_xy) - onp.mean(pred_out_np_xy)
            pred_out_np_xy = pred_out_np_xy + energy_offset
            if out_func is not None:
                out_np = out_np + energy_offset

        # Plot quadrature data points.
        plot_kwargs["markersize"] = plot_kwargs.pop("markersize", 10)
        if self._energy:
            plt.plot(
                x_data_xy,
                pred_out_np_xy,
                "x",
                alpha=0.3,
                c="r",
                **plot_kwargs,
                label="Network (simulation invariants)",
            )
        else:
            plt.plot(
                x_data_xy,
                pred_out_np_xy[:, 1],
                "x",
                alpha=0.3,
                c="r",
                **plot_kwargs,
                label="Network (simulation invariants)",
            )
        plot_kwargs.pop("markersize")

        # Plot full extrapolated output.
        if out_func is not None:
            if out_np.shape[1:] == ():
                exp_lines = plt.plot(
                    x_data,
                    out_np,
                    ".",
                    alpha=0.3,
                    **plot_kwargs,
                    label="Network (generalization)",
                )
            else:
                exp_lines = []
                (exp_line,) = plt.plot(
                    x_data,
                    out_np[:, 1],
                    ".",
                    alpha=0.3,
                    **plot_kwargs,
                    label="Network (generalization)",
                )
                exp_lines.append(exp_line)

        plt.plot(
            x_data, true_out_np, "g--", linewidth=8, **plot_kwargs, label="Ground truth"
        )
        plt.title(title)

        if out_func is not None:
            plt.legend()

        plt.xlabel(r"tr $\epsilon^2$")
        if self._energy:
            plt.ylabel("$E$")
        else:
            plt.ylabel("$f_2$")
