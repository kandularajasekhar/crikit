#!/bin/bash -xe
coverage run --source=crikit,pyadjoint_utils  -m pytest --reruns 3 --junit-xml=junit-tests-dir.xml tests
coverage report
coverage html
python -m pytest --reruns 3 --doctest-modules --rootdir tests --junit-xml=junit-src-doctests.xml crikit
cd docs && make doctest
